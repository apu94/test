package com.ba.gas.monkey.security.admin;

import com.ba.gas.monkey.dtos.user.LoggedInUserBean;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class UserDetailsImpl implements UserDetails {

    private final LoggedInUserBean userInfo;

    public UserDetailsImpl(LoggedInUserBean userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : this.userInfo.getAuthorities()) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.userInfo.getPassword();
    }

    @Override
    public String getUsername() {
        return this.userInfo.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.userInfo.getStatus();
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.userInfo.getStatus();
    }

    public List<String> getAccessMenus() {
        return this.userInfo.getMenus();
    }
}
