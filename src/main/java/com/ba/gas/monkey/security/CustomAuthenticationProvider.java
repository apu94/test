package com.ba.gas.monkey.security;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.user.LoggedInUserBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.user.UserInfoService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Objects;

@Component
@AllArgsConstructor
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final UserInfoService userInfoService;
    private final CustomerService customerService;

    private final PasswordEncoder passwordEncoder;

    private final ModelMapper modelMapper;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
        String[] arr = userName.split(":");
        String type = arr[0];
        String loginCredential = arr[1];
        LoggedInUserBean userBean = null;
        if (AppConstant.USER_TYPE_ADMIN.equalsIgnoreCase(type)) {
            UserInfoBean userInfoBean = userInfoService.getByEmailAddress(loginCredential)
                    .orElseThrow(() -> new BadCredentialsException("You are not a registered user. Contact with admin"));
            if (!userInfoBean.getStatus()) {
                throw new BadCredentialsException("You are not a registered user. Contact with admin");
            }
            if (StringUtils.isBlank(authentication.getCredentials().toString())
                    || !passwordEncoder.matches(authentication.getCredentials().toString(), userInfoBean.getPassword())) {
                throw new BadCredentialsException("You have entered wrong password");
            }
            userBean = modelMapper.map(userInfoBean, LoggedInUserBean.class);
        } else if (AppConstant.APP_OTP_LOGIN.equalsIgnoreCase(type)) {
            userBean = customerService.getCustomerByPhoneNo(loginCredential);
            if (Objects.isNull(userBean) || !userBean.getStatus()) {
                throw new BadCredentialsException("You are not a registered user. Register now and start your journey");
            }
        } else {
            userBean = customerService.getCustomerByPhoneNo(loginCredential);
            if (Objects.isNull(userBean) || !userBean.getStatus()) {
                throw new BadCredentialsException("You are not a registered user. Register now and start your journey");
            }
            if (StringUtils.isBlank(authentication.getCredentials().toString())
                    || !passwordEncoder.matches(authentication.getCredentials().toString(), userBean.getPassword())) {
                throw new BadCredentialsException("You have entered wrong password");
            }
        }
        return new UsernamePasswordAuthenticationToken(new UserDetailsImpl(userBean), password, new ArrayList<>());
    }

    @Override

    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
