package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.PushNotification;

public interface PushNotificationRepository extends BaseRepository<PushNotification> {
}
