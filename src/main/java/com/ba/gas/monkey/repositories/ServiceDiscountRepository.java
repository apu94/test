package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.ServiceDiscount;

public interface ServiceDiscountRepository extends BaseRepository<ServiceDiscount>  {

}
