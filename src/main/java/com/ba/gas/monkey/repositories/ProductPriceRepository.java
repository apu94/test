package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.models.ProductPrice;

import java.util.Optional;

public interface ProductPriceRepository extends BaseRepository<ProductPrice> {
    Optional<ProductPrice> findByProduct(Product updatedProduct);
}
