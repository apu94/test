package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.AttachmentType;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.BrandReview;

import java.util.List;

public interface BrandReviewRepository extends BaseRepository<BrandReview> {

    List<BrandReview> findByBrand(Brand brand);
}
