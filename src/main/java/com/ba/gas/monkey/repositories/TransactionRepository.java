package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionRepository extends BaseRepository<Transaction> {

    @Query("SELECT t FROM Transaction t WHERE t.partner=:partner ORDER BY t.dateCreated DESC")
    List<Transaction> findByPartner(Customer partner, Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE t.partner=:partner ORDER BY t.dateCreated DESC")
    List<Transaction> findByPartner(Customer partner);
}
