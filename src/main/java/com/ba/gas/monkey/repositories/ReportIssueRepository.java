package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.ReportIssue;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface ReportIssueRepository extends BaseRepository<ReportIssue> {
    List<ReportIssue> findByCustomerId(String id, Sort sort);
}
