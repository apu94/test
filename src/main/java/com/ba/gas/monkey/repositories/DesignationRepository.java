package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Department;
import com.ba.gas.monkey.models.Designation;

public interface DesignationRepository extends BaseRepository<Designation> {

}
