package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Product;
import com.ba.gas.monkey.models.ProductSize;
import com.ba.gas.monkey.models.ProductValveSize;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends BaseRepository<Product> {

//    @Query("SELECT p FROM Product p WHERE p.brand=:brand AND p.productSize=:productSize AND p.productValveSize=:productValveSize")
    List<Product> findByBrandAndProductSizeAndProductValveSize(Brand brand, ProductSize productSize, ProductValveSize productValveSize);

    @Query(value = "SELECT s FROM Product s WHERE s.brand.id in(:brandIds)")
    List<Product> findAllByBrandIds(List<String> brandIds);

    @Query(value = "SELECT s FROM Product s WHERE s.productSize.id in(:productSizeIds)")
    List<Product> findAllByProductSizeIds(List<String> productSizeIds);

    @Query(value = "SELECT s FROM Product s WHERE s.productValveSize.id in(:productValveSizeIds)")
    List<Product> findAllByProductValveSizeIds(List<String> productValveSizeIds);
}
