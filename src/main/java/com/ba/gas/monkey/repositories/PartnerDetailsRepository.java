package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.PartnerDetails;

import java.util.List;
import java.util.Optional;

public interface PartnerDetailsRepository extends BaseRepository<PartnerDetails> {

    Optional<PartnerDetails> findByCustomerId(String customerId);

}
