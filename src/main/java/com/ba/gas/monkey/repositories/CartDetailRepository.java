package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Cart;
import com.ba.gas.monkey.models.CartDetail;

import java.util.List;

public interface CartDetailRepository extends BaseRepository<CartDetail> {
    List<CartDetail> findByCart(Cart cart);
}
