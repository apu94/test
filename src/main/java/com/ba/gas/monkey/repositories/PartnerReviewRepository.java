package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.BrandReview;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.PartnerReview;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PartnerReviewRepository extends BaseRepository<PartnerReview> {

    List<PartnerReview> findByPartner(Customer partner);

    @Query("SELECT p FROM PartnerReview p WHERE p.partner.id=:partnerId")
    List<PartnerReview> findByPartnerId(String partnerId);
}
