package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.CancelReason;

public interface CancelReasonRepository extends BaseRepository<CancelReason>  {

}
