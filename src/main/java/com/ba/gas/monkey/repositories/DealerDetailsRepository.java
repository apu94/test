package com.ba.gas.monkey.repositories;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.models.BrandImage;
import com.ba.gas.monkey.models.DealerDetails;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface DealerDetailsRepository extends BaseRepository<DealerDetails> {

    DealerDetails findByCustomerId(String customerId);

    @Query("SELECT d FROM DealerDetails d WHERE d.customerId=:customerId")
    Optional<DealerDetails> findByDealerId(String customerId);
}
