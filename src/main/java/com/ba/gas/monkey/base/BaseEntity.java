package com.ba.gas.monkey.base;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@MappedSuperclass
@RequiredArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", updatable = false, nullable = false)
    private String id;

    @Version
    @Column(name = "VERSION")
    private Integer version = 1;

    @CreatedDate
    @Column(name = "DATE_CREATED")
    private Instant dateCreated;

    @LastModifiedDate
    @Column(name = "DATE_MODIFIED")
    private Instant dateModified;

    @Column(name = "UPDT_ID")
    private String updtId;

    @Column(name = "STATUS", columnDefinition = "BIT default 0", length = 1)
    private Boolean status = Boolean.TRUE;

}
