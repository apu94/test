package com.ba.gas.monkey.resources.admin.dealer;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.dealer.DealerCreateBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.dealer.DealerDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class DealerResource {

    private final DealerDetailsService service;

    @GetMapping("dealer")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getDealerListBeans(pageable), HttpStatus.OK);
    }

    @GetMapping("dealer/{id}")
    public ResponseEntity<?> getDealerDetails(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getDealerDetails(id), HttpStatus.OK);
    }

    @PutMapping("dealer/approved/{id}")
    public ResponseEntity<?> approvedDealer(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.approvedDealer(id), HttpStatus.OK);
    }

    @PostMapping("dealer")
    public ResponseEntity<?> saveDealer(@RequestBody @Valid DealerCreateBean bean) {
        return new ResponseEntity<>(service.saveDealer(bean), HttpStatus.OK);
    }

    @PutMapping("dealer/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }

    @PutMapping("dealer/{id}")
    public ResponseEntity<?> updateDealer(@PathVariable("id") String id, @RequestBody @Valid DealerCreateBean bean) {
        return new ResponseEntity<>(service.updateDealer(id,bean), HttpStatus.OK);
    }

}
