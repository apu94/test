package com.ba.gas.monkey.resources.dealer;

import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@DealerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('DEALER')")
public class DealerOrderResource {

    private final OrderService orderService;

    @GetMapping("completed-order")
    public ResponseEntity<?> dealerCompletedOrderList() {
        return new ResponseEntity<>(orderService.dealerCompletedOrderList(), HttpStatus.OK);
    }

    @GetMapping("running-order")
    public ResponseEntity<?> dealerOrderList() {
        return new ResponseEntity<>(orderService.dealerOrderList(), HttpStatus.OK);
    }

    @GetMapping("past-order-list-current-month/{id}")
    public ResponseEntity<?> DealerPastOrderListCurrentMonth(@PathVariable String id) {
        return new ResponseEntity<>(orderService.getDealerPastOrderListCurrentMonth(id), HttpStatus.OK);
    }
    @GetMapping("transaction-summary/{id}")
    public ResponseEntity<?> dealerTransactionSummaryMonth(@PathVariable String id, @RequestParam String date ) {
        return new ResponseEntity<>(orderService.getDealerTransactionSummaryMonth(id, date), HttpStatus.OK);
    }
}
