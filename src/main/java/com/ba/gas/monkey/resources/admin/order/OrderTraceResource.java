package com.ba.gas.monkey.resources.admin.order;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.order.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@AdminV1API
@AllArgsConstructor
public class OrderTraceResource {

    private final OrderService orderService;

    @GetMapping("order-trace/{id}")
    public ResponseEntity<?> orderTrace(@PathVariable("id") String id) {
        return new ResponseEntity<>(orderService.orderTrace(id), HttpStatus.OK);
    }
}
