package com.ba.gas.monkey.resources.admin.geo;

import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.dtos.thana.ThanaBean;
import com.ba.gas.monkey.dtos.thana.ThanaCreateBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.ThanaService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class ThanaResource {

    private final ThanaService thanaService;


    @GetMapping("thana/{districtId}")
    public ResponseEntity<?> getDropdownList(@PathVariable String districtId) {
        return new ResponseEntity<>(thanaService.getDropdownList(districtId), HttpStatus.OK);
    }

    @GetMapping("thana-list/{districtId}")
    public ResponseEntity<?> getThanaList(Pageable pageable, @PathVariable("districtId") String districtId) {
        return new ResponseEntity<>(thanaService.getThanaList(pageable,districtId), HttpStatus.OK);
    }

    @GetMapping("thana-by-id/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id){
        return new ResponseEntity<>(thanaService.getByOid(id),HttpStatus.OK);
    }

    @PostMapping("thana")
    public ResponseEntity<?> createThana(@RequestBody @Validated ThanaCreateBean bean){
        return new ResponseEntity<>(thanaService.createThana(bean),HttpStatus.OK);
    }

    @DeleteMapping("thana/{id}")
    public ResponseEntity<?> deleteDistrict(@PathVariable("id") String id){
        return new ResponseEntity<>(thanaService.deleteThana(id),HttpStatus.OK);
    }

    @PutMapping("thana/{id}")
    public ResponseEntity<?> updateThana(@PathVariable("id") String id, @RequestBody @Valid ThanaCreateBean bean){
        return new ResponseEntity<>(thanaService.updateThana(id,bean),HttpStatus.OK);
    }

}
