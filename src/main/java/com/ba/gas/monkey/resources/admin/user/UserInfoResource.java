package com.ba.gas.monkey.resources.admin.user;

import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.user.UserInfoEditBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.services.user.UserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;


@AdminV1API
@AllArgsConstructor
public class UserInfoResource {

    private final UserInfoService service;

    @GetMapping("users")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getList(pageable), HttpStatus.OK);
    }

    @GetMapping("users/{id}")
    public ResponseEntity<?> getByUserId(@PathVariable @Min(36) String id) {
        return new ResponseEntity<>(service.getByUserId(id), HttpStatus.OK);
    }

    @PutMapping("users/{id}")
    public ResponseEntity<?> getByUserId(@PathVariable @Min(36) String id, @RequestBody @Valid UserInfoEditBean userInfoBean) {
        return new ResponseEntity<>(service.updateUser(id, userInfoBean), HttpStatus.OK);
    }
}
