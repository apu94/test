package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.attachment.AttachmentTypeBean;
import com.ba.gas.monkey.services.AttachmentTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class AttachmentTypeResource {

    private AttachmentTypeService service;

    @GetMapping("attachment-type")
    public ResponseEntity<?> getConfigurationData(Pageable pageable) {
        return new ResponseEntity<>(service.getAttachmentList(pageable), HttpStatus.OK);
    }

    @GetMapping("attachment-type/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @PostMapping("attachment-type")
    public ResponseEntity<?> getUpdateConfigurationData(@RequestBody @Valid AttachmentTypeBean bean) {
        return new ResponseEntity<>(service.createAttachmentType(bean), HttpStatus.OK);
    }

    @PutMapping("attachment-type/{id}")
    public ResponseEntity<?> getUpdateConfigurationData(@PathVariable("id") String id, @RequestBody @Valid AttachmentTypeBean bean) {
        return new ResponseEntity<>(service.updateAttachmentType(id, bean), HttpStatus.OK);
    }
}
