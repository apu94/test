package com.ba.gas.monkey.resources.customer.notification;

import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER','PARTNER','DEALER')")
public class CustomerNotificationResource {

    private final NotificationService notificationService;

    @GetMapping("notification-list/{id}")
    public ResponseEntity<?> notificationList(@PathVariable String id, @RequestParam("deviceType") String deviceType) {
        return new ResponseEntity<>(notificationService.getNotificationList(id, deviceType), HttpStatus.OK);
    }

    @GetMapping("notification-seen/{id}")
    public ResponseEntity<?> notificationSeen(@PathVariable String id) {
        return new ResponseEntity<>(notificationService.customerNotificationSeen(id), HttpStatus.OK);
    }
}
