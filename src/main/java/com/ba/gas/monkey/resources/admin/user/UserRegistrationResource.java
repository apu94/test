package com.ba.gas.monkey.resources.admin.user;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.dtos.user.UserRegistrationBean;
import com.ba.gas.monkey.services.registration.UserRegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class UserRegistrationResource {

    private final UserRegistrationService service;

    @PostMapping("/user-registration")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRegistrationBean registrationDTO) {
        service.userRegistration(registrationDTO);
        return ResponseEntity.ok("User created");
    }
}
