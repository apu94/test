package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.mobilebanking.MobileBankingInfoBean;
import com.ba.gas.monkey.services.MobileBankingInfoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class MobileBankingInfoResource {

    private final MobileBankingInfoService bankingInfoService;

    @GetMapping("mobile-banking-info")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(bankingInfoService.getListBeans(pageable), HttpStatus.OK);
    }

    @GetMapping("mobile-banking-info/{id}")
    public ResponseEntity<?> getByOid(@PathVariable("id") String id) {
        return new ResponseEntity<>(bankingInfoService.getByOid(id), HttpStatus.OK);
    }

    @GetMapping("mobile-banking-info/dropdown-list")
    public ResponseEntity<?> getList() {
        return new ResponseEntity<>(bankingInfoService.getDropdownList(), HttpStatus.OK);
    }

    @PostMapping("mobile-banking-info")
    public ResponseEntity<?> saveMobileBankingInfo(@RequestBody @Valid MobileBankingInfoBean bean) {
        return new ResponseEntity<>(bankingInfoService.saveMobileBankingInfo(bean), HttpStatus.OK);
    }

    @PutMapping("mobile-banking-info/{id}")
    public ResponseEntity<?> updateMobileBankingInfo(@PathVariable("id") String id, @RequestBody @Valid MobileBankingInfoBean bean) {
        return new ResponseEntity<>(bankingInfoService.updateMobileBankingInfo(id, bean), HttpStatus.OK);
    }

    @DeleteMapping("mobile-banking-info/{id}")
    public ResponseEntity<?> deleteMobileBankingInfo(@PathVariable("id") String id) {
        return new ResponseEntity<>(bankingInfoService.deleteMobileBankingInfo(id), HttpStatus.OK);
    }
}
