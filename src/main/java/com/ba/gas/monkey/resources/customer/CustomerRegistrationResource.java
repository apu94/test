package com.ba.gas.monkey.resources.customer;

import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.services.customer.CustomerRegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
public class CustomerRegistrationResource {

    private final CustomerRegistrationService service;

    @PostMapping("customer-registration")
    public ResponseEntity<?> createCustomerRegistration(@RequestBody @Valid CustomerRegistrationBean bean) {
        return new ResponseEntity<>(service.createCustomerRegistration(bean), HttpStatus.OK);
    }
}
