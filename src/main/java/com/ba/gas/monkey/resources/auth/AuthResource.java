package com.ba.gas.monkey.resources.auth;

import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.*;
import com.ba.gas.monkey.dtos.changepassword.PasswordChangedBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.otp.OTPBean;
import com.ba.gas.monkey.dtos.otp.OTPLoginBean;
import com.ba.gas.monkey.dtos.otp.OTPRequestBean;
import com.ba.gas.monkey.dtos.otp.OTPVerifyBean;
import com.ba.gas.monkey.dtos.user.UserInfoBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.paylod.request.AppLogoutRequestBean;
import com.ba.gas.monkey.paylod.request.LogoutRequestBean;
import com.ba.gas.monkey.paylod.response.JwtTokenResponseBean;
import com.ba.gas.monkey.paylod.request.LoginRequestBean;
import com.ba.gas.monkey.paylod.response.RefreshTokenResponseBean;
import com.ba.gas.monkey.paylod.request.TokenRefreshRequestBean;
import com.ba.gas.monkey.dtos.refreshtoken.RefreshTokenBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.security.admin.UserDetailsImpl;
import com.ba.gas.monkey.services.DeviceTokenService;
import com.ba.gas.monkey.services.LogoutService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.refreshtoken.RefreshTokenService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.JwtUtils;
import com.ba.gas.monkey.web.WebService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@AuthV1APIResource
@AllArgsConstructor
public class AuthResource {

    private final JwtUtils jwtUtils;
    private final AuthenticationManager authenticationManager;
    private final RefreshTokenService refreshTokenService;
    private final UserInfoService userInfoService;
    private final CustomerService customerService;
    private final DeviceTokenService deviceTokenService;
    private final LogoutService logoutService;
    private final WebService webService;

    @PostMapping("login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestBean loginRequestBean) {
        String username = "ADMIN:" + loginRequestBean.getUsername();
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequestBean.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        RefreshTokenBean refreshToken = refreshTokenService.createRefreshToken(userDetails.getUserInfo().getId());
        userInfoService.updateLastAccess(userDetails.getUserInfo().getId());
        DeviceTokenBean deviceTokenBean = DeviceTokenBean.builder().token(loginRequestBean.getToken())
                .module(AppConstant.USER_TYPE_ADMIN).userId(userDetails.getUserInfo().getId()).deviceType(loginRequestBean.getDeviceType().toUpperCase()).build();
        deviceTokenService.saveDeviceToken(deviceTokenBean);
        return ResponseEntity.ok(JwtTokenResponseBean.builder().token(jwt).type(AppConstant.TOKEN_TYPE).refreshToken(refreshToken.getToken()).build());
    }
    /*https://www.bezkoder.com/spring-boot-refresh-token-jwt/*/

    @PostMapping("/refresh-token")
    public ResponseEntity<?> refreshToken(@Valid @RequestBody TokenRefreshRequestBean request) {
        String requestRefreshToken = request.getRefreshToken();
        RefreshTokenBean refreshTokenBean = refreshTokenService.getByToken(requestRefreshToken).orElseThrow(() -> new ServiceExceptionHolder.RefreshTokenException("Refresh token is not in database!"));
        RefreshTokenBean tokenBean = refreshTokenService.verifyExpiration(refreshTokenBean);
        userInfoService.updateLastAccess(tokenBean.getUserId());
        UserInfoBean userDetails = userInfoService.getUserDetails(tokenBean.getUserId());
        String token = jwtUtils.generateTokenFromUsername(userDetails);
        return ResponseEntity.ok(RefreshTokenResponseBean.builder().accessToken(token).refreshToken(requestRefreshToken).tokenType(AppConstant.TOKEN_TYPE).build());
    }

    @PostMapping("app/login")
    public ResponseEntity<?> appLogin(@Valid @RequestBody LoginRequestBean loginRequestBean) {
        String username = "APP:" + loginRequestBean.getUsername();
        Optional<Customer> optionalCustomer = customerService.getAppUserByPhoneNo(loginRequestBean.getUsername());
        if (optionalCustomer.isEmpty()) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.APP_USER_NOT_FOUND_EXCEPTION, "You are not registered yet, to register please click the register button");
        }
        if (optionalCustomer.get().getCustomerType().getCustomerType().equalsIgnoreCase(AppConstant.CUSTOMER_TYPE)) {
            String userType = Optional.ofNullable(loginRequestBean.getUserType())
                    .orElseThrow(() -> new ServiceExceptionHolder.CustomException(AppConstant.APP_USER_TYPE_NOT_FOUND_EXCEPTION, "You are not registered yet, to register please click the register button"));
            if (!userType.equalsIgnoreCase(AppConstant.CUSTOMER_TYPE)) {
                throw new ServiceExceptionHolder.CustomException(AppConstant.APP_USER_TYPE_NOT_FOUND_EXCEPTION, "Type mismatch");
            }
        }
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequestBean.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        RefreshTokenBean refreshToken = refreshTokenService.createRefreshTokenForApp(userDetails.getUserInfo().getId());
        customerService.updateLastAccess(userDetails.getUserInfo().getId());
        CustomerBean customerBean = customerService.customerDetails(userDetails.getUserInfo().getId());
        AppLogoutRequestBean logoutRequestBean = new AppLogoutRequestBean();
        logoutRequestBean.setDeviceType(loginRequestBean.getDeviceType());
        logoutRequestBean.setUserId(customerBean.getId());
        deviceTokenService.removeDeviceToken(logoutRequestBean);
        DeviceTokenBean deviceTokenBean = DeviceTokenBean.builder().token(loginRequestBean.getToken())
                .module(AppConstant.APP_MODULE).userId(customerBean.getId()).deviceType(loginRequestBean.getDeviceType().toUpperCase()).build();
        deviceTokenService.saveDeviceToken(deviceTokenBean);
        return ResponseEntity.ok(JwtTokenResponseBean.builder().token(jwt)
                .type(AppConstant.TOKEN_TYPE)
                .role(customerBean.getTypeBean().getCustomerType())
                .refreshToken(refreshToken.getToken()).build());
    }

    @PostMapping("app/refresh-token")
    public ResponseEntity<?> appRefreshToken(@Valid @RequestBody TokenRefreshRequestBean request) {
        String requestRefreshToken = request.getRefreshToken();
        RefreshTokenBean refreshTokenBean = refreshTokenService.getByToken(requestRefreshToken).orElseThrow(() -> new ServiceExceptionHolder.RefreshTokenException("Refresh token is not in database!"));
        RefreshTokenBean tokenBean = refreshTokenService.verifyExpiration(refreshTokenBean);
        customerService.updateLastAccess(tokenBean.getUserId());
        CustomerBean customerBean = customerService.customerDetails(tokenBean.getUserId());
        String token = jwtUtils.generateTokenForApp(customerBean);
        return ResponseEntity.ok(RefreshTokenResponseBean.builder().accessToken(token)
                .refreshToken(requestRefreshToken)
                .role(customerBean.getTypeBean().getCustomerType())
                .tokenType(AppConstant.TOKEN_TYPE).build());
    }

    @PostMapping("logout")
    public ResponseEntity<?> adminLogout(@Valid @RequestBody LogoutRequestBean request) {
        return ResponseEntity.ok(logoutService.adminLogout(request));
    }

    @PostMapping("app/logout")
    public ResponseEntity<?> appLogout(@Valid @RequestBody AppLogoutRequestBean request) {
        return ResponseEntity.ok(logoutService.logout(request));
    }

    @PostMapping("app/send-otp")
    public ResponseEntity<?> sendOTP(@RequestBody @Valid OTPRequestBean otpRequestBean) {
        return ResponseEntity.ok(webService.sendOTP(otpRequestBean));
    }

    @PostMapping("app/send-login-otp")
    public ResponseEntity<?> sendLoginOTP(@RequestBody @Valid OTPRequestBean otpRequestBean) {
        return ResponseEntity.ok(webService.sendLoginOTP(otpRequestBean));
    }

    @PostMapping("forget-password")
    public ResponseEntity<?> forgetPassword(@RequestBody @Valid UserEmailBean userEmailBean) {
        return ResponseEntity.ok(webService.forgetPassword(userEmailBean));
    }

    @PostMapping("app/otp-login")
    public ResponseEntity<?> otpBasedLogin(@RequestBody @Valid OTPLoginBean loginBean) {
        if (!AppConstant.SESSION_DATA_MAP.containsKey(loginBean.getAccessToken())) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "Invalid token");
        }
        String phone = (String) AppConstant.SESSION_DATA_MAP.get(loginBean.getAccessToken());
        String username = "APP-OTP:" + phone;
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, "password"));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        RefreshTokenBean refreshToken = refreshTokenService.createRefreshTokenForApp(userDetails.getUserInfo().getId());
        customerService.updateLastAccess(userDetails.getUserInfo().getId());
        CustomerBean customerBean = customerService.customerDetails(userDetails.getUserInfo().getId());
        AppLogoutRequestBean logoutRequestBean = new AppLogoutRequestBean();
        logoutRequestBean.setDeviceType(loginBean.getDeviceType());
        logoutRequestBean.setUserId(customerBean.getId());
        deviceTokenService.removeDeviceToken(logoutRequestBean);
        DeviceTokenBean deviceTokenBean = DeviceTokenBean.builder().token(loginBean.getToken())
                .module(AppConstant.APP_MODULE).userId(customerBean.getId()).deviceType(loginBean.getDeviceType().toUpperCase()).build();
        deviceTokenService.saveDeviceToken(deviceTokenBean);
        AppConstant.SESSION_DATA_MAP.remove(loginBean.getAccessToken());
        return ResponseEntity.ok(JwtTokenResponseBean.builder().token(jwt)
                .type(AppConstant.TOKEN_TYPE)
                .role(customerBean.getTypeBean().getCustomerType())
                .refreshToken(refreshToken.getToken()).build());

    }

    @PostMapping("app/forget-password")
    public ResponseEntity<?> forgetPassword(@RequestBody @Valid ForgetPasswordBean passwordBean) {
        if (!passwordBean.getPassword().equalsIgnoreCase(passwordBean.getConfirmPassword())) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "Password Mismatch");
        }
        boolean hasOTP = AppConstant.SESSION_DATA_MAP.containsKey(passwordBean.getToken());
        if (!hasOTP) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "Invalid token");
        }
        String phone = (String) AppConstant.SESSION_DATA_MAP.get(passwordBean.getToken());
        AppConstant.SESSION_DATA_MAP.remove(passwordBean.getToken());
        Optional<CustomerBean> optional = customerService.getByPhoneNo(phone);
        if (optional.isEmpty()) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "No user found");
        }
        customerService.passwordUpdate(optional.get(), passwordBean);
        return ResponseEntity.ok(PasswordChangedBean.builder().msg(AppConstant.SUCCESS).build());
    }

    @PostMapping("app/otp/verify")
    public ResponseEntity<?> sendVerify(@RequestBody @Valid OTPVerifyBean verifyBean) {
        String key = verifyBean.getSessionId() + "#" + verifyBean.getPhoneNo();
        boolean hasOTP = AppConstant.SESSION_DATA_MAP.containsKey(key);
        if (!hasOTP) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "No OTP found");
        }
        OTPBean otpBean = (OTPBean) AppConstant.SESSION_DATA_MAP.get(key);
        if (!otpBean.getOtp().equalsIgnoreCase(verifyBean.getOtp())) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "Your OTP is not correct");
        }
        if (otpBean.getExpiryTime().isBefore(Instant.now())) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.OTP_TOKEN_NOT_FOUND_EXCEPTION, "Your OTP time expired");
        }
        AppConstant.SESSION_DATA_MAP.remove(key);
        String uuid = UUID.randomUUID().toString();
        AppConstant.SESSION_DATA_MAP.put(uuid, verifyBean.getPhoneNo());
        return ResponseEntity.ok(PasswordChangedBean.builder().token(uuid).msg(AppConstant.SUCCESS).build());
    }

}
