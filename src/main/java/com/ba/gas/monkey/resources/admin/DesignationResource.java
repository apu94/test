package com.ba.gas.monkey.resources.admin;


import com.ba.gas.monkey.dtos.StatusBean;
import com.ba.gas.monkey.dtos.department.DepartmentBean;
import com.ba.gas.monkey.dtos.designation.DesignationBean;
import com.ba.gas.monkey.services.department.DepartmentService;
import com.ba.gas.monkey.services.designation.DesignationService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class DesignationResource {

    private final DesignationService service;

    @GetMapping("designation/dropdown-list")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("designation")
    public ResponseEntity<?> getList(Pageable pageable) {
        return new ResponseEntity<>(service.getDesignationList(pageable), HttpStatus.OK);
    }

    @GetMapping("designation/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @PostMapping("designation")
    public ResponseEntity<?> createDesignation(@RequestBody @Valid DesignationBean bean) {
        return new ResponseEntity<>(service.createDesignation(bean), HttpStatus.OK);
    }

    @PutMapping("designation/{id}")
    public ResponseEntity<?> updateDesignation(@PathVariable("id") String id, @RequestBody @Valid DesignationBean bean) {
        return new ResponseEntity<>(service.updateDesignation(id, bean), HttpStatus.OK);
    }

    @PutMapping("designation/update-status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @RequestBody @Valid StatusBean statusBean) {
        return new ResponseEntity<>(service.updateRowStatus(id, statusBean.getStatus()), HttpStatus.OK);
    }
}
