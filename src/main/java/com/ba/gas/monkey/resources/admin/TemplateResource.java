package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.template.TemplateBean;
import com.ba.gas.monkey.services.TemplateService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class TemplateResource {

    private final TemplateService service;

    @GetMapping("template")
    public ResponseEntity<?> getTemplateList(Pageable pageable) {
        return new ResponseEntity<>(service.getTemplateList(pageable), HttpStatus.OK);
    }

    @GetMapping("template/{id}")
    public ResponseEntity<?> getByOid(@PathVariable String id) {
        return new ResponseEntity<>(service.getByOid(id), HttpStatus.OK);
    }

    @GetMapping("template/dropdown-list")
    public ResponseEntity<?> getTemplateDropdownList() {
        return new ResponseEntity<>(service.getDropdownList(), HttpStatus.OK);
    }

    @PostMapping("template")
    public ResponseEntity<?> create(@RequestBody @Valid TemplateBean typeBean) {
        return new ResponseEntity<>(service.save(typeBean), HttpStatus.OK);
    }

    @PutMapping("template/{id}")
    public ResponseEntity<?> updateTemplate(@PathVariable String id, @RequestBody @Valid TemplateBean typeBean) {
        return new ResponseEntity<>(service.updateTemplate(id, typeBean), HttpStatus.OK);
    }

}
