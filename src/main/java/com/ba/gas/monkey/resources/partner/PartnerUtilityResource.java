package com.ba.gas.monkey.resources.partner;

import com.ba.gas.monkey.dtos.DistanceBean;
import com.ba.gas.monkey.dtos.UpdateLocationBean;
import com.ba.gas.monkey.dtos.userstatus.UserStatusBean;
import com.ba.gas.monkey.services.AttachmentTypeService;
import com.ba.gas.monkey.services.MobileBankingInfoService;
import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.dealer.DealerDetailsService;
import com.ba.gas.monkey.services.partner.PartnerDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@PartnerV1API
@AllArgsConstructor
public class PartnerUtilityResource {
    private final MobileBankingInfoService mobileBankingInfoService;
    private final PartnerDetailsService partnerDetailsService;
    private final AttachmentTypeService attachmentTypeService;
    private final BrandService brandService;
    private final DealerDetailsService dealerDetailsService;
    private final CustomerService customerService;

    @GetMapping("mobile-banking-info/dropdown-list")
    public ResponseEntity<?> getMobileDropdownList() {
        return new ResponseEntity<>(mobileBankingInfoService.getDropdownList(), HttpStatus.OK);
    }

    @PostMapping("nearest-dealer")
    public ResponseEntity<?> nearestDealer(@RequestBody @Valid DistanceBean distanceBean) {
        return new ResponseEntity<>(dealerDetailsService.getNearestDealerList(distanceBean), HttpStatus.OK);
    }

    @GetMapping("attachment-type/dropdown-list")
    public ResponseEntity<?> getAttachmentTypeDropdownList() {
        return new ResponseEntity<>(attachmentTypeService.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("brand/dropdown-list")
    public ResponseEntity<?> getBrandDropdownList() {
        return new ResponseEntity<>(brandService.dropdownList(), HttpStatus.OK);
    }

    @PostMapping("nearest-partner")
    public ResponseEntity<?> nearestDealer(@RequestBody @Valid DistanceBean distanceBean, Pageable pageable) {
        return new ResponseEntity<>(partnerDetailsService.getNearestPartnerByDistance(distanceBean, pageable), HttpStatus.OK);
    }


    @PostMapping("update-status")
    @PreAuthorize("hasAnyAuthority('PARTNER')")
    public ResponseEntity<?> updateStatus(@RequestBody @Valid UserStatusBean userStatusBean) {
        return new ResponseEntity<>(customerService.updateStatus(userStatusBean), HttpStatus.OK);
    }

    @GetMapping("status")
    @PreAuthorize("hasAnyAuthority('PARTNER')")
    public ResponseEntity<?> updateStatus() {
        return new ResponseEntity<>(customerService.status(), HttpStatus.OK);
    }

    @PostMapping("update-location")
    @PreAuthorize("hasAnyAuthority('PARTNER')")
    public ResponseEntity<?> updateLocation(@RequestBody @Valid UpdateLocationBean locationBean) {
        return new ResponseEntity<>(customerService.updateLocation(locationBean), HttpStatus.OK);
    }

}
