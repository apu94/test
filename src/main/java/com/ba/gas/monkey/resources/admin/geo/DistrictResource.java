package com.ba.gas.monkey.resources.admin.geo;

import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.DistrictService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class DistrictResource {

    private final DistrictService districtService;

    @GetMapping("district")
    public ResponseEntity<?> getDropdownList() {
        return new ResponseEntity<>(districtService.getDropdownList(), HttpStatus.OK);
    }

    @GetMapping("district-list")
    public ResponseEntity<?> getDistrictList(Pageable pageable) {
        return new ResponseEntity<>(districtService.getDistrictList(pageable), HttpStatus.OK);
    }

    @GetMapping("district/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id){
        return new ResponseEntity<>(districtService.getByOid(id),HttpStatus.OK);
    }

    @PostMapping("district")
    public ResponseEntity<?> createDistrict(@Valid @RequestBody DistrictBean bean){
        return new ResponseEntity<>(districtService.createDistrict(bean),HttpStatus.OK);
    }

    @DeleteMapping("district/{id}")
    public ResponseEntity<?> deleteDistrict(@PathVariable("id") String id){
        return new ResponseEntity<>(districtService.deleteDistrict(id),HttpStatus.OK);
    }

    @PutMapping("district/{id}")
    public ResponseEntity<?> updateDistrict(@PathVariable("id") String id, @RequestBody @Valid DistrictBean bean){
        return new ResponseEntity<>(districtService.updateDistrict(id,bean),HttpStatus.OK);
    }

}
