package com.ba.gas.monkey.resources.admin.customer;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.customer.CustomerAttachmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

@AdminV1API
@AllArgsConstructor
public class CustomerAttachmentResource {

    private final CustomerAttachmentService service;

    @DeleteMapping("customer-attachment/{id}")
    public ResponseEntity<?> getList(@PathVariable String id) {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }
}
