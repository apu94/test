package com.ba.gas.monkey.resources.admin.customer;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@AdminV1API
@AllArgsConstructor
public class CustomerTypeResource {

    private final CustomerTypeService service;

    @GetMapping("customer-type/dropdown-list")
    public ResponseEntity<?> getCustomerTypeDropdownList() {
        return new ResponseEntity<>(service.getCustomerTypeDropdownList(), HttpStatus.OK);
    }

    @GetMapping("customer-type/dropdown-list-all")
    public ResponseEntity<?> getCustomerTypeDropdownAllList() {
        return new ResponseEntity<>(service.getCustomerTypeDropdownAllList(), HttpStatus.OK);
    }


}
