package com.ba.gas.monkey.resources.admin;

import com.ba.gas.monkey.dtos.transaction.TransactionCreateBean;
import com.ba.gas.monkey.services.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class TransactionResource {

    private final TransactionService service;

    @PostMapping("transaction/payment")
    public ResponseEntity<?> create(@RequestBody @Valid TransactionCreateBean createBean) {
        return new ResponseEntity<>(service.payment(createBean), HttpStatus.OK);
    }
}
