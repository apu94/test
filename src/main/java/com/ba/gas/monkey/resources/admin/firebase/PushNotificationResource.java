package com.ba.gas.monkey.resources.admin.firebase;


import com.ba.gas.monkey.dtos.firebase.Note;
import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.firebase.FirebaseMessagingService;
import com.ba.gas.monkey.services.notification.AppNotificationService;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@AdminV1API
@AllArgsConstructor
public class PushNotificationResource {

    @Autowired
    private final FirebaseMessagingService firebaseMessagingService;
    @Autowired
    private final AppNotificationService appNotificationService;

    @PostMapping("/send-notification")
    @ResponseBody
    public String sendNotification(@RequestBody Note note,
                                   @RequestParam String token) throws FirebaseMessagingException {

        String action = note.getDataValue().get("type");
        if(action.equalsIgnoreCase("action")){
            String orderId = note.getDataValue().get("orderId");
            String partnerId = note.getDataValue().get("partnerId");
            return appNotificationService.adminSendOrderActionNotification(orderId, partnerId);
        }else {
            return firebaseMessagingService.sendNotification(note, token);
        }
    }
}
