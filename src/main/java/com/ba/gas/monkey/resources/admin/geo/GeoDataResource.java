package com.ba.gas.monkey.resources.admin.geo;

import com.ba.gas.monkey.resources.admin.AdminV1API;
import com.ba.gas.monkey.services.ClusterService;
import com.ba.gas.monkey.services.DistrictService;
import com.ba.gas.monkey.services.ThanaService;
import com.ba.gas.monkey.services.department.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@AdminV1API
@AllArgsConstructor
public class GeoDataResource {

}
