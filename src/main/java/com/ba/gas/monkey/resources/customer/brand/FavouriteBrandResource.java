package com.ba.gas.monkey.resources.customer.brand;

import com.ba.gas.monkey.dtos.FavouriteBrandBean;
import com.ba.gas.monkey.dtos.brand.FavouriteBrandRequestBean;
import com.ba.gas.monkey.dtos.product.ProductDetailFilterBean;
import com.ba.gas.monkey.resources.customer.CustomerV1API;
import com.ba.gas.monkey.services.FavouriteBrandService;
import com.ba.gas.monkey.services.product.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@CustomerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('CUSTOMER')")
public class FavouriteBrandResource {

    private final FavouriteBrandService service;
    private final ProductService productService;

    @PostMapping("favourite-brand")
    public ResponseEntity<?> favouriteBrand(@RequestBody @Valid FavouriteBrandRequestBean bean) {
        return new ResponseEntity<>(service.favouriteBrand(bean), HttpStatus.OK);
    }

    @GetMapping("favourite-brand")
    public ResponseEntity<?> favouriteBrandByCustomer() {
        return new ResponseEntity<>(productService.favouriteBrandByCustomer(), HttpStatus.OK);
    }
}
