package com.ba.gas.monkey.resources.utility;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
@RequestMapping("v1/utility")
public @interface UtilityV1APIResource {
    @AliasFor(annotation = Component.class)
    String value() default "";
}
