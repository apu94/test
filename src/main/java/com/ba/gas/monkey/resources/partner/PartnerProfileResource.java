package com.ba.gas.monkey.resources.partner;

import com.ba.gas.monkey.dtos.ProfileUpdateBean;
import com.ba.gas.monkey.resources.dealer.DealerV1API;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.dealer.DealerDetailsService;
import com.ba.gas.monkey.services.partner.PartnerDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@PartnerV1API
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('PARTNER')")
public class PartnerProfileResource {

    private final PartnerDetailsService partnerDetailsService;

    @GetMapping("profile")
    public ResponseEntity<?> profile() {
        return new ResponseEntity<>(partnerDetailsService.profile(), HttpStatus.OK);
    }

    @PostMapping("profile")
    public ResponseEntity<?> profileUpdate(@RequestBody @Valid ProfileUpdateBean bean) {
        return new ResponseEntity<>(partnerDetailsService.profileUpdate(bean), HttpStatus.OK);
    }

    @DeleteMapping("profile")
    public ResponseEntity<?> deletePartner() {
        return new ResponseEntity<>(partnerDetailsService.deletePartner(), HttpStatus.OK);
    }
}
