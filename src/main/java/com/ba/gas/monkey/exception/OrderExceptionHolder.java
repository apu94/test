package com.ba.gas.monkey.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class OrderExceptionHolder {

    public final static int EXCEPTION_ORDER_CANCELED = 1003;

    @Getter
    @RequiredArgsConstructor
    public static class ServiceException extends RuntimeException {
        private final int code;
        private final String message;
    }

    public static class ResourceNotFoundException extends ServiceException {
        public ResourceNotFoundException(int code, String message) {
            super(code, message);
        }
    }

    public static class ServerNotFoundException extends ResourceNotFoundException {
        public ServerNotFoundException(final String msg, final int code) {
            super(code, msg);
        }
    }

    public static class OrderCanceledException extends ResourceNotFoundException {
        public OrderCanceledException(final String msg) {
            super(EXCEPTION_ORDER_CANCELED, msg);
        }
    }

    public static class CustomException extends ResourceNotFoundException {
        public CustomException(int code, String msg) {
            super(code, msg);
        }
    }
}
