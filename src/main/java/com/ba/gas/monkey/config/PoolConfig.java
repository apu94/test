package com.ba.gas.monkey.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@EnableAsync
@EnableScheduling
@Configuration
public class PoolConfig {

    private static final int MAX_POOL_SIZE = 15;
    private static final int CORE_POLL_SIZE = 15;

    @Bean(name = "asyncExecutor")
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(MAX_POOL_SIZE);
        executor.setCorePoolSize(CORE_POLL_SIZE);
        executor.setThreadNamePrefix("TH-SEND-NOTIFICATION-");
        executor.initialize();
        return executor;
    }

    @Bean(name = "asyncPendingExecutor")
    public Executor asyncPendingExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(MAX_POOL_SIZE);
        executor.setCorePoolSize(CORE_POLL_SIZE);
        executor.setThreadNamePrefix("TH-PENDING-NOTIFICATION-");
        executor.initialize();
        return executor;
    }
}
