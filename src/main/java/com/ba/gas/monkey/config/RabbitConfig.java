package com.ba.gas.monkey.config;

import com.ba.gas.monkey.listener.RabbitMessageListener;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;


@Configuration
public class RabbitConfig {
    public static final String AMQP_EXCHANGE = "exchange-gas-monkey";
    private static final String DEAD_LETTER_EXCHANGE = "dead.letter.exchange-gas-monkey";
    private static final String DEAD_LETTER_QUEUE = "dead.letter.queue-gas-monkey";
    public final static String MESSAGE_QUEUE_ORDER = "message-queue-order";
    public final static String MESSAGE_QUEUE_PAYMENT = "message-queue-payment";
    public final static String MESSAGE_QUEUE_PROMOTIONAL = "message-queue-promotional";
    public final static String MESSAGE_QUEUE_SCHEDULE = "message-queue-schedule";
    public final static String MESSAGE_QUEUE_SUPPORT = "message-queue-support";
    public final static String MESSAGE_QUEUE_FINALACK = "message-queue-finalack-gas-monkey";
//    public final static String MESSAGE_QUEUE_RECONCILE = "message.queue.reconcile-gas-monkey";
    public final static String MESSAGE_QUEUE_DEBUG= "message-queue-debug-gas-monkey";

    @Value("${message.broker.final.ack.queue.prefetch.count:10}")
    int finalAckPrefetchCount;
    @Value("${message.broker.final.ack.queue.consumer.concurrency.min:5}")
    int finalAckMinConcurrentChannel;
    @Value("${message.broker.final.ack.queue.consumer.concurrency.max:10}")
    int finalAckMaxConcurrentChannel;


    @Bean
    Queue queueOrder() {
        return new Queue(MESSAGE_QUEUE_ORDER, true);
    }

    @Bean
    Queue queuePayment() {
        return new Queue(MESSAGE_QUEUE_PAYMENT, true);
    }

    @Bean
    Queue queuePromotional() {
        return new Queue(MESSAGE_QUEUE_PROMOTIONAL, true);
    }

    @Bean
    Queue queueSchedule() {
        return new Queue(MESSAGE_QUEUE_SCHEDULE, true);
    }

    @Bean
    Queue queueSupport() {
        return new Queue(MESSAGE_QUEUE_SUPPORT, true);
    }

//    @Bean
//    Queue queueReconcile() {
//        return new Queue(MESSAGE_QUEUE_RECONCILE, true);
//    }

    @Bean
    Queue queueDebugDeathQueue() {
        return new Queue(MESSAGE_QUEUE_DEBUG, true);
    }

    @Bean
    Queue queueFinalAck() {
        return QueueBuilder.durable(MESSAGE_QUEUE_FINALACK)
                .withArgument("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE)
                .withArgument("x-dead-letter-routing-key",DEAD_LETTER_QUEUE)
                .build();
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(AMQP_EXCHANGE);
    }

    @Bean
    Binding bindDeadLetterQueueWtihDeadLetterExchange(Queue deadLetterQueue, TopicExchange deadLetterExchange) {
        return BindingBuilder.bind(deadLetterQueue).to(deadLetterExchange).with(DEAD_LETTER_QUEUE);
    }
    @Bean
    TopicExchange deadLetterExchange() {
        return new TopicExchange(DEAD_LETTER_EXCHANGE);
    }
    @Bean
    Queue deadLetterQueue() {
        return QueueBuilder.durable(DEAD_LETTER_QUEUE).build();
    }

    @Bean
    Binding bindingOrder(@Qualifier("queueOrder") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGE_QUEUE_ORDER);
    }

    @Bean
    Binding bindingPayment(@Qualifier("queuePayment") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGE_QUEUE_PAYMENT);
    }

    @Bean
    Binding bindingSchedule(@Qualifier("queueSchedule") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGE_QUEUE_SCHEDULE);
    }

    @Bean
    Binding bindingPromotional(@Qualifier("queuePromotional") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGE_QUEUE_PROMOTIONAL);
    }

    @Bean
    Binding bindingSupport(@Qualifier("queueSupport") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGE_QUEUE_SUPPORT);
    }

    @Bean
    Binding bindingDebugDeathQueue(@Qualifier("queueDebugDeathQueue") Queue queue, TopicExchange deadLetterExchange) {
        return BindingBuilder.bind(queue).to(deadLetterExchange).with(DEAD_LETTER_QUEUE);
    }

//    @Bean
//    Binding bindingReconcileOrderQueue(Queue queueReconcile, TopicExchange exchange) {
//        return BindingBuilder.bind(queueReconcile).to(exchange).with(MESSAGE_QUEUE_ORDER);
//    }
//
//    @Bean
//    Binding bindingReconcilePaymentQueue(Queue queueReconcile, TopicExchange exchange) {
//        return BindingBuilder.bind(queueReconcile).to(exchange).with(MESSAGE_QUEUE_PAYMENT);
//    }
//
//    @Bean
//    Binding bindingReconcilePromotionalQueue(Queue queueReconcile, TopicExchange exchange) {
//        return BindingBuilder.bind(queueReconcile).to(exchange).with(MESSAGE_QUEUE_PROMOTIONAL);
//    }
//
//    @Bean
//    Binding bindingReconcileScheduleQueue(Queue queueReconcile, TopicExchange exchange) {
//        return BindingBuilder.bind(queueReconcile).to(exchange).with(MESSAGE_QUEUE_SCHEDULE);
//    }
//    @Bean
//    Binding bindingReconcileSupportQueue(Queue queueReconcile, TopicExchange exchange) {
//        return BindingBuilder.bind(queueReconcile).to(exchange).with(MESSAGE_QUEUE_SUPPORT);
//    }


    @Bean
    Binding bindingFinalAck(Queue queueFinalAck, TopicExchange exchange) {
        return BindingBuilder.bind(queueFinalAck).to(exchange).with(MESSAGE_QUEUE_FINALACK);
    }

    @Bean
    SimpleMessageListenerContainer containerFinalAck(ConnectionFactory connectionFactory,
                                                MessageListenerAdapter listenerAdapterFinalAck) {

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setAdviceChain(interceptor());
        container.setPrefetchCount(finalAckPrefetchCount);
        container.setConcurrentConsumers(finalAckMinConcurrentChannel);
        container.setMaxConcurrentConsumers(finalAckMaxConcurrentChannel);
        container.setQueueNames(MESSAGE_QUEUE_FINALACK);
        container.setMessageListener(listenerAdapterFinalAck);
        return container;
    }

    @Bean
    SimpleMessageListenerContainer containerDeathQueue(ConnectionFactory connectionFactory,
                                                MessageListenerAdapter listenerAdapterDeathQueue) {

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setAdviceChain(interceptor());
        container.setPrefetchCount(finalAckPrefetchCount);
        container.setConcurrentConsumers(finalAckMinConcurrentChannel);
        container.setMaxConcurrentConsumers(finalAckMaxConcurrentChannel);
        container.setQueueNames(DEAD_LETTER_QUEUE);
        container.setMessageListener(listenerAdapterDeathQueue);
        return container;
    }

//    @Bean
//    SimpleMessageListenerContainer containerReconcileQueue(ConnectionFactory connectionFactory,
//                                                       MessageListenerAdapter listenerAdapterReconcileQueue) {
//
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory);
//        container.setAdviceChain(interceptor());
//        container.setPrefetchCount(finalAckPrefetchCount);
//        container.setConcurrentConsumers(finalAckMinConcurrentChannel);//1
//        container.setMaxConcurrentConsumers(finalAckMaxConcurrentChannel);//1
//        container.setQueueNames(MESSAGE_QUEUE_RECONCILE);
//
//        container.setMessageListener(listenerAdapterReconcileQueue);
//        return container;
//    }



    @Bean
    MessageListenerAdapter listenerAdapterFinalAck(RabbitMessageListener receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessageFinalAck");
    }

    @Bean
    MessageListenerAdapter listenerAdapterDeathQueue(RabbitMessageListener receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessageDeathQueue");
    }

//    @Bean
//    MessageListenerAdapter listenerAdapterReconcileQueue(MessageListener receiver) {
//        return new MessageListenerAdapter(receiver, "receiveMessageReconcileQueue");
//    }



    @Bean
    public RetryOperationsInterceptor interceptor() {
        return RetryInterceptorBuilder.stateless()
                .maxAttempts(10)
                .backOffOptions(5000, 1.5, 30000)
                .recoverer(new RejectAndDontRequeueRecoverer())
                .build();
    }
}
