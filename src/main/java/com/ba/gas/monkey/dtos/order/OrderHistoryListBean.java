package com.ba.gas.monkey.dtos.order;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;

@Data
public class OrderHistoryListBean {
    private String partnerName;
    private Instant dateTime;
    private String status;
    private String orderStatus;
}
