package com.ba.gas.monkey.dtos.department;

import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class DepartmentListBean extends RowInfoBean {
    private String name;
    private String status;
}
