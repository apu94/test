package com.ba.gas.monkey.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SupportTicketResponseBean {
    private String details;
    private String ticketNo;
    private String issueTypeId;
    private String customerId;
}
