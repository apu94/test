package com.ba.gas.monkey.dtos.changepassword;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PasswordChangedBean {
    private String msg;
    private String token;
}
