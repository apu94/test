package com.ba.gas.monkey.dtos.otp;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OTPLoginBean {
    @NotBlank
    private String token;
    @NotBlank
    private String deviceType;
    @NotBlank
    private String accessToken;
}
