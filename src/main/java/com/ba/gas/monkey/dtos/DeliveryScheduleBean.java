package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.models.DeliverySchedule;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Data
public class DeliveryScheduleBean implements IRequestBodyDTO {
    @ValidEntityId(DeliverySchedule.class)
    private String id;
    @NotNull
    private LocalTime startTime;
    @NotNull
    private LocalTime endTime;
    @NotNull
    private Integer slotDurationInMin;
    @NotNull
    private Boolean status;
    @NotNull
    private Boolean enabledTimeSlot;
}
