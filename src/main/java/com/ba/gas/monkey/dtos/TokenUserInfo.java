package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class TokenUserInfo {
    private String id;
}
