package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.models.Product;
import lombok.Data;

@Data
public class OrderProductBean implements IRequestBodyDTO {
    private String id;
    private OrderInfo orderInfo;
    private Product buyProduct;
    private Product returnProduct;
    private Integer quantity;
    private Double refillPrice;
    private Double emptyCylinderPrice;
    private Double packagePrice;
    private Double productPurchagePrice;
    private Double returnEmptyCylinderPrice;
    private Double exchangeAmount;
}
