package com.ba.gas.monkey.dtos.partner;

import com.ba.gas.monkey.dtos.customer.CustomerBasicInfoBean;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PartnerReviewListBean {
    private LocalDateTime reviewDate;
    private Double reviewRating;
    private Boolean reviewRead;
    private String description;
    private CustomerBasicInfoBean customerBasicInfoBean;
    private String orderLocation;
    private String cluster;
}
