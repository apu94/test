package com.ba.gas.monkey.dtos.reportissue;

import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.IssueType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class CreateReportIssueRequestBean {
    private String details;
    @ValidEntityId(IssueType.class)
    private String issueTypeId;
    @ValidEntityId(Customer.class)
    private String customerId;
}
