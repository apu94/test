package com.ba.gas.monkey.dtos.coupon;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CouponBean implements IRequestBodyDTO {
    private String id;
    private String couponCode;
    private Double amount;
    private String couponType;
    private String couponUsageType;
    private Boolean status;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer usageLimit;
    private Integer couponUsed;
}
