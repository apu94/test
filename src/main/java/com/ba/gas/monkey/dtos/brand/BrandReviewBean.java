package com.ba.gas.monkey.dtos.brand;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class BrandReviewBean implements IRequestBodyDTO {
    private LocalDateTime reviewDate;
    @NotNull
    private Double reviewRating;
    private Boolean reviewRead;
    private String description;
    @ValidEntityId(Brand.class)
    private String brandId;
//    @ValidEntityId(Customer.class)
//    private String customerId;
}
