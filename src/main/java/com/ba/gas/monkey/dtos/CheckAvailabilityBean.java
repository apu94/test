package com.ba.gas.monkey.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CheckAvailabilityBean {
    private Boolean available;
}
