package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.dtos.customer.CustomerBean;
import lombok.Data;

import java.time.Month;
import java.time.MonthDay;
import java.util.Map;

@Data
public class DealerDashboardBean implements IRequestBodyDTO {
    private Integer orderReceived;
    private Double totalDeliveryVsOrder;
    private Integer deliveredThisMonth;
    private Double totalSalesThisMonth;
    Map<MonthDay, Double> salesDayOnMonth;
    Map<Month, Double> salesOnMonth;
    String date;
    private CustomerBean customerBean;
}
