package com.ba.gas.monkey.dtos.pushnotification;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PushNotificationResponseBean {
    private String msg;
    private Boolean success;
}
