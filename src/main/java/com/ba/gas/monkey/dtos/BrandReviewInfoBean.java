package com.ba.gas.monkey.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BrandReviewInfoBean {
    private String customerName;
    private String photoLink;
    private Double ratingPoint;
    private String description;
}
