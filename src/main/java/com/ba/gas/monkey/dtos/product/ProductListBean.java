package com.ba.gas.monkey.dtos.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductListBean {
    private String id;
    private String nameEn;
    private String nameBn;
    private String brandNameEn;
    private String brandNameBn;
    private String productSizeEn;
    private String productSizeBn;
    private String valveSizeEn;
    private String valveSizeBn;
    private Double refillPrice;
    private Double packagePrice;
    private Double emptyCylinderPrice;
    private String companyName;
    private String productPhoto;
    private String productCode;
    private String status;
    private String featureProduct;
    private String offerProduct;
}
