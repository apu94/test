package com.ba.gas.monkey.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ForgetPasswordBean {
    @NotBlank
    private String token;
    @NotBlank
    private String password;
    @NotBlank
    private String confirmPassword;
}
