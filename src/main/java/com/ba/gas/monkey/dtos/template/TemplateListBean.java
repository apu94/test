package com.ba.gas.monkey.dtos.template;

import lombok.Data;

@Data
public class TemplateListBean {
    private String id;
    private String title;
    private String description;
    private String appToUser;
    private String status;
}
