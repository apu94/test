package com.ba.gas.monkey.dtos.cart;

import com.ba.gas.monkey.dtos.product.ProductBean;
import lombok.Data;

@Data
public class CartDetailsResponseBean {
    private String id;
    private ProductBean productBuy;
    private ProductBean productReturn;
    private Boolean status;
    private Integer quantity;
}
