package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.models.CancelReason;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrderStatusUpdateBean implements Serializable {
    @ValidEntityId(OrderInfo.class)
    private String orderId;
    @ValidEntityId(Customer.class)
    private String partnerId;
    @ValidEntityId(Customer.class)
    private String dealerId;
    private String orderStatus;
    private CancelReason cancelReason;
}
