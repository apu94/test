package com.ba.gas.monkey.dtos.appmenu;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class AppMenuBean implements IRequestBodyDTO {
    private String id;
    private String title;
    private String tagName;
}
