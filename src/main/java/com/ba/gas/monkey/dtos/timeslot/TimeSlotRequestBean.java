package com.ba.gas.monkey.dtos.timeslot;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TimeSlotRequestBean {
    private LocalDate date;
}
