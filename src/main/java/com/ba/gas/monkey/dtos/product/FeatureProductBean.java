package com.ba.gas.monkey.dtos.product;

import lombok.Data;

@Data
public class FeatureProductBean {
    private String id;
    private String nameEn;
    private String nameBn;
    private String brandNameEn;
    private String brandNameBn;
}
