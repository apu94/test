package com.ba.gas.monkey.dtos.sliderImage;

import lombok.Data;

@Data
public class SliderImageListBean {
    private String id;
    private String imageLink;
    private String status;
    private String title;
    private String imageLinkId;
}
