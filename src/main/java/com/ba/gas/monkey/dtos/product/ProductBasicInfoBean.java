package com.ba.gas.monkey.dtos.product;

import lombok.Data;

@Data
public class ProductBasicInfoBean {
    private String brandName;
    private String image;
    private ProductPriceBean productPrice;
    private ProductSizeBean productSize;
    private ProductValveSizeBean valveSize;
}
