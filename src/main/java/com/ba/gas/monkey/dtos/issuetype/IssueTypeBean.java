package com.ba.gas.monkey.dtos.issuetype;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.dtos.RowInfoBean;
import lombok.Data;

@Data
public class IssueTypeBean extends RowInfoBean implements IRequestBodyDTO {
    private String title;
    private String description;
    private Boolean status;
}
