package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class ProfileUpdateBean {
    private String customerName;
    private String emailAddress;
    private String photoLink;
}
