package com.ba.gas.monkey.dtos.user;

import com.ba.gas.monkey.models.Department;
import com.ba.gas.monkey.models.Designation;
import com.ba.gas.monkey.models.GroupInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRegistrationBean {

    @ValidEntityId(GroupInfo.class)
    private String groupId;
    @NotBlank
    private String userFullName;
    @ValidEntityId(Department.class)
    private String departmentId;
    @ValidEntityId(Designation.class)
    private String designationId;
    @Email
    @Size(max = 50)
    @NotBlank(message = "email cannot be empty")
    private String email;
    @NotBlank(message = "mobile cannot be empty")
    private String mobile;
    @Size(min = 8, max = 40)
    @NotBlank(message = "password cannot be empty")
    private String password;
    @Size(min = 8, max = 40)
    @NotBlank(message = "confirm-password cannot be empty")
    private String confirmPassword;

}
