package com.ba.gas.monkey.dtos.partner;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class PartnerReviewBean implements IRequestBodyDTO {
    private LocalDateTime reviewDate;
    @NotNull
    private Double reviewRating;
    private Boolean reviewRead;
    private String description;
    @ValidEntityId(OrderInfo.class)
    private String orderId;
    private String customerId;
    @ValidEntityId(Customer.class)
    private String partnerId;
}
