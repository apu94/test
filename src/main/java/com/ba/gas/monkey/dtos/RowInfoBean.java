package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class RowInfoBean {
    private String id;
    private Integer version;

}
