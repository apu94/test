package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class CreateOrderRequestBean implements IRequestBodyDTO {
    @ValidEntityId(Customer.class)
    private String customerId;
    @ValidEntityId(Cart.class)
    private String cartId;
    private String orderStatus;
    private Double subTotal;
    private Double vat;
    private Double serviceCharge;
    private Double discountAmount;
    private Double totalExchange;
    private Double totalConvenienceFee;
    private Double total;
    private String couponCode;
    private Double couponValue;
    private String note;
    private Boolean regularDelivery;
    private LocalDate deliveryDate;
    private String deliverySlot;
    private String deliveryPaymentType;
    private String deliveryMapAddress;
    private Double deliveryLat;
    private Double deliveryLong;
    @ValidEntityId(District.class)
    private String districtId;
    @ValidEntityId(Thana.class)
    private String thanaId;
    @ValidEntityId(Cluster.class)
    private String clusterId;
    private String deliveryArea;
    private Boolean lift;
    private String floor;
    private Boolean isHome;
    private Double partnerCharge;
    private Double ownerCharge;
    private Boolean paymentStatus;
}
