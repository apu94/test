package com.ba.gas.monkey.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentInfoBean {
    private String id;
    private Double totalPayable;
}
