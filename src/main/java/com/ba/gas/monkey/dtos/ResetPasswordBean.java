package com.ba.gas.monkey.dtos;

import com.ba.gas.monkey.models.UserInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ResetPasswordBean {

    @ValidEntityId(UserInfo.class)
    private String userId;
    @NotBlank
    private String password;
    @NotBlank
    private String confirmPassword;

}
