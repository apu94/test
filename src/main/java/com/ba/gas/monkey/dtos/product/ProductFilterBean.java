package com.ba.gas.monkey.dtos.product;

import lombok.Data;

import java.util.List;

@Data
public class ProductFilterBean {
    private List<String> brandIds;
    private List<String> productSizeIds;
    private List<String> productValveSizeIds;
    private List<String> ratings;
}
