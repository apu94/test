package com.ba.gas.monkey.dtos.brand;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PopularBrandBean implements IRequestBodyDTO {

    private String id;
    private String brandNameEn;
    private String brandNameBn;
    private String rating;
    private String review;
    private String imageUrl;
    private Boolean favoriteStatus;
    private Product product;
    private Boolean discountEnabled;
    private String discountType;
    private Double discountValue;
    private LocalDate discountStartDate;
    private LocalDate discountEndDate;

}
