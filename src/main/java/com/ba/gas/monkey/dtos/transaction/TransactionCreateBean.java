package com.ba.gas.monkey.dtos.transaction;

import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TransactionCreateBean {
    @NotNull
    private String note;
    @NotNull
    private Double amount;
    @ValidEntityId(Customer.class)
    private String customerId;
}
