package com.ba.gas.monkey.dtos.attachment;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AttachmentTypeListBean implements IRequestBodyDTO {
    private String id;
    private String attachmentName;
    private String status;
}
