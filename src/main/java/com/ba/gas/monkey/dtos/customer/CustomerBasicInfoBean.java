package com.ba.gas.monkey.dtos.customer;

import lombok.Data;

@Data
public class CustomerBasicInfoBean {
    private String photoLink;
    private String customerName;
}
