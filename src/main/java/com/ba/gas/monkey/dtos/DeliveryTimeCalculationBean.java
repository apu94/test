package com.ba.gas.monkey.dtos;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class DeliveryTimeCalculationBean {

    private LocalDate dateTime;
    private String slot;
}
