package com.ba.gas.monkey.dtos.appmenu;

import com.ba.gas.monkey.models.AppMenu;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class AssignMenuCreateBean {
    @ValidEntityId(AppMenu.class)
    private String menuId;
}
