package com.ba.gas.monkey.dtos.order;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class ForwardOrderBean implements IRequestBodyDTO {
    @ValidEntityId(OrderInfo.class)
    private String orderId;
    @ValidEntityId(Customer.class)
    private String partnerId;
}
