package com.ba.gas.monkey.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoggedInUserBean {
    private String id;
    private String email;
    private String mobile;
    private String password;
    private Boolean status;
    private List<String> authorities;
    private List<String> menus;
}
