package com.ba.gas.monkey.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StatusBean {
    @NotNull
    private Boolean status;
}
