package com.ba.gas.monkey.dtos.pushnotification;

import lombok.Data;

@Data
public class PushNotificationListBean  {
    private String id;
    private String subject;
    private String customerType;
    private String templateName;
}
