package com.ba.gas.monkey.dtos.userstatus;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserStatusBean {
    @NotNull
    private Boolean status;
}
