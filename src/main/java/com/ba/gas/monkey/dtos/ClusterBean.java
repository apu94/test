package com.ba.gas.monkey.dtos;

import lombok.Data;

@Data
public class ClusterBean implements IRequestBodyDTO {
    private String id;
    private String name;
    private Boolean status;
}
