package com.ba.gas.monkey.dtos;

public interface IdHolderRequestBodyDTO {

    String getId();

    void setId(String id);
}
