package com.ba.gas.monkey.dtos.cart;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CartResponseBean {
    private String id;
    private List<CartDetailsResponseBean> cartDetailList;
}
