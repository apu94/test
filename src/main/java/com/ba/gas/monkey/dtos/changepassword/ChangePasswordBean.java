package com.ba.gas.monkey.dtos.changepassword;

import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChangePasswordBean {
    @NotBlank
    private String oldPassword;
    @NotBlank
    private String newPassword;
    @NotBlank
    private String confirmPassword;
}
