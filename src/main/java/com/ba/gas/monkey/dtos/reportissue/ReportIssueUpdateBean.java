package com.ba.gas.monkey.dtos.reportissue;

import lombok.Data;

@Data
public class ReportIssueUpdateBean {
    private String id;
    private String note;
}
