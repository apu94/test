package com.ba.gas.monkey.dtos.reportissue;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.IssueType;
import com.ba.gas.monkey.validators.ValidEntityId;
import lombok.Data;

@Data
public class ReportIssueBean implements IRequestBodyDTO {
    private Integer issueStatus;
    private String details;
    private String note;
    private String ticketNo;
    @ValidEntityId(IssueType.class)
    private String issueTypeId;
    @ValidEntityId(Customer.class)
    private String customerId;
}
