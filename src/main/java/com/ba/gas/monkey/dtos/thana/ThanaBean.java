package com.ba.gas.monkey.dtos.thana;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class ThanaBean implements IRequestBodyDTO {
    private String id;
    private String name;
    private Boolean status;
}
