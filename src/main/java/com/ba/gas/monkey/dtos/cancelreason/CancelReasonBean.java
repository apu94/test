package com.ba.gas.monkey.dtos.cancelreason;

import com.ba.gas.monkey.dtos.IRequestBodyDTO;
import lombok.Data;

@Data
public class CancelReasonBean implements IRequestBodyDTO {
    private String id;
    private String title;
    private Boolean status;
}
