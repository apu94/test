package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "product_size")
@EqualsAndHashCode(callSuper = true)
public class ProductSize extends BaseEntity {
    @Column(name = "NAME_EN")
    private String nameEn;
    @Column(name = "NAME_BN")
    private String nameBn;
}
