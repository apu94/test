package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "slider_image")
@EqualsAndHashCode(callSuper = true)
public class SliderImage extends BaseEntity {

    @Column(name = "TITLE")
    private String title;
    @Column(name = "IMAGE_LINK")
    private String imageLink;
    @Column(name = "IMAGE_LINK_ID")
    private String imageLinkId;

}
