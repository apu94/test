package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "notification_details")
@EqualsAndHashCode(callSuper = true)
public class NotificationDetails extends BaseEntity {
    @JsonBackReference
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NOTIFICATION_ID", referencedColumnName = "ID")
    private Notification notification;
    @Column(name = "SUBJECT")
    private String subject;
    @Column(name = "CONTENT", columnDefinition = "TEXT")
    private String content;
}
