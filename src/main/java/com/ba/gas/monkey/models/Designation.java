package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "designation")
@EqualsAndHashCode(callSuper = true)
public class Designation extends BaseEntity {

    @Column(name = "NAME")
    private String name;
}
