package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "cancel_reason")
@EqualsAndHashCode(callSuper = true)
public class CancelReason extends BaseEntity {
    @Column(name = "TITLE")
    private String title;
}
