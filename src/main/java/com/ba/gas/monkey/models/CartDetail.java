package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "cart_detail")
@EqualsAndHashCode(callSuper = true)
public class CartDetail extends BaseEntity {
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "CART_ID", referencedColumnName = "ID")
    private Cart cart;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "BUY_PRODUCT_ID", referencedColumnName = "ID")
    private Product buyProduct;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "RETURN_PRODUCT_ID", referencedColumnName = "ID")
    private Product returnProduct;
    @Column(name = "QUANTITY")
    private Integer quantity;
}
