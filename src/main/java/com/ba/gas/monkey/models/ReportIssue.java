package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "report_issue")
@EqualsAndHashCode(callSuper = true)
public class ReportIssue extends BaseEntity {
    @Column(name = "ISSUE_STATUS")
    private Integer issueStatus;
    @Column(name = "TICKET_NO")
    private String ticketNo;
    @Column(name = "DETAILS", columnDefinition = "TEXT")
    private String details;
    @Column(name = "NOTE", columnDefinition = "TEXT")
    private String note;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "ISSUE_TYPE_ID", referencedColumnName = "ID")
    private IssueType issueType;
    @JsonManagedReference
    @OneToOne
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
    private Customer customer;
}
