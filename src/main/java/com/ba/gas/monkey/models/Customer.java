package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "customer")
@EqualsAndHashCode(callSuper = true)
public class Customer extends BaseEntity {

    @Column(name = "CUSTOMER_NAME")
    private String customerName;
    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;
    @Column(name = "PHONE_NO")
    private String phoneNo;
    @Column(name = "COMPANY_NAME")
    private String companyName;
    @OneToOne
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "ID")
    private District district;
    @OneToOne
    @JoinColumn(name = "THANA_ID", referencedColumnName = "ID")
    private Thana thana;
    @OneToOne
    @JoinColumn(name = "CLUSTER_ID", referencedColumnName = "ID")
    private Cluster cluster;
    @Column(name = "AREA")
    private String area;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "GPS_ADDRESS")
    private String gpsAddress;
    @Column(name = "REWARD_POINT", columnDefinition = "default 0")
    private Integer rewardPoint;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "LATITUDE")
    private Double latitude;
    @Column(name = "LONGITUDE")
    private Double longitude;
    @Column(name = "FLOOR_NO")
    private String floorNo;
    @Column(name = "LIFT_ALLOWED")
    private Boolean liftAllowed = Boolean.FALSE;
    @Column(name = "PHOTO_LINK")
    private String photoLink;
    @Column(name = "USER_STATUS", columnDefinition = "ENUM")
    private String userStatus;
    @OneToOne
    @JoinColumn(name = "TYPE", referencedColumnName = "ID")
    private CustomerType customerType;
    @Column(name = "APPROVED")
    private Boolean approved = Boolean.FALSE;
    @LastModifiedDate
    @Column(name = "LAST_ACCESS")
    private LocalDateTime lastAccess;
}
