package com.ba.gas.monkey.models;

import com.ba.gas.monkey.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "cluster")
@EqualsAndHashCode(callSuper = true)
public class Cluster extends BaseEntity {

    @Column(name = "NAME")
    private String name;
    @OneToOne
    @JoinColumn(name = "THANA_ID", referencedColumnName = "ID")
    private Thana thana;

}
