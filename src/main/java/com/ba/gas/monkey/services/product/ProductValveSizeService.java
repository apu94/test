package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.CommonResponseBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.product.ProductValveSizeBean;
import com.ba.gas.monkey.models.ProductSize;
import com.ba.gas.monkey.models.ProductValveSize;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("productValveSizeService")
public class ProductValveSizeService extends BaseService<ProductValveSize, ProductValveSizeBean> {
    public ProductValveSizeService(BaseRepository<ProductValveSize> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public Page<ProductValveSizeListBean> getProductValveSizeList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated")));
        Page<ProductValveSize> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent()
                .stream()
                .map(this::getProductValveSizeListBean)
                .sorted(Comparator.comparing(ProductValveSizeListBean::getStatus))
                .collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private ProductValveSizeListBean getProductValveSizeListBean( ProductValveSize productValveSize) {
        ProductValveSizeListBean bean = getModelMapper().map(productValveSize, ProductValveSizeListBean.class);
        bean.setStatus(productValveSize.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public List<DropdownDTO> getDropdownList() {
        List<ProductValveSize> list = getRepository().findAll(Sort.by("nameEn"));
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(ProductValveSize valveSize) {
        DropdownDTO dropdownDTO = getModelMapper().map(valveSize, DropdownDTO.class);
        dropdownDTO.setValue(valveSize.getNameEn());
        dropdownDTO.setValueBn(valveSize.getNameBn());
        return dropdownDTO;
    }

    public Object createValveSize(ProductValveSizeBean bean) {
         ProductValveSize productValveSize = convertForCreate(bean);
         return create(productValveSize);
    }

    public Object deleteValveSize(String id) {
        ProductValveSize productValveSize = getRepository().getById(id);
        productValveSize.setStatus(Boolean.FALSE);
        update(id,productValveSize);
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }


    public Object updateValveSize(String id, ProductValveSizeBean bean) {
        ProductValveSize productValveSize = getRepository().getById(id);
        BeanUtils.copyProperties(bean, productValveSize, AppConstant.IGNORE_PROPERTIES);
        return update(id,productValveSize);
    }
}
