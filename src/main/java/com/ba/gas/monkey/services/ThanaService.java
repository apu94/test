package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.CommonResponseBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.thana.ThanaBean;
import com.ba.gas.monkey.dtos.thana.ThanaCreateBean;
import com.ba.gas.monkey.dtos.thana.ThanaListBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.District;
import com.ba.gas.monkey.models.Thana;
import com.ba.gas.monkey.repositories.ThanaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("thanaService")
public class ThanaService extends BaseService<Thana, ThanaBean> {

    private final DistrictService districtService;

    public ThanaService(BaseRepository<Thana> repository, ModelMapper modelMapper, DistrictService districtService) {
        super(repository, modelMapper);
        this.districtService = districtService;
    }

    public List<DropdownDTO> getDropdownList(String districtId) {
        District district = districtService.getRepository().findById(districtId).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No district found by id :" + districtId));
        List<Thana> thanas = ((ThanaRepository) getRepository())
                .findByDistrict(district)
                .stream()
                .filter(Thana::getStatus)
                .sorted(Comparator.comparing(Thana::getName))
                .collect(Collectors.toUnmodifiableList());
        return thanas.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Thana thana) {
        DropdownDTO dto = getModelMapper().map(thana, DropdownDTO.class);
        dto.setValue(thana.getName());
        return dto;
    }

    public Page<ThanaListBean> getThanaList(Pageable pageable, String districtId) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated")));
        District district = districtService.getRepository()
                .findById(districtId)
                .orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No district found by id :" + districtId));

        Page<Thana> page = ((ThanaRepository) getRepository())
                .findByDistrict(district, pageRequest);

        return new PageImpl<>(page.getContent()
                .stream()
                .sorted(Comparator.comparing(Thana::getStatus).reversed())
                .map(this::getThanaListBean)
                .collect(Collectors.toUnmodifiableList()),
                page.getPageable(), page.getTotalElements());
    }

    private ThanaListBean getThanaListBean(Thana thana) {
        ThanaListBean bean = getModelMapper().map(thana, ThanaListBean.class);
        bean.setStatus(thana.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public Object createThana(ThanaCreateBean bean) {
        Thana thana = getModelMapper().map(bean, Thana.class);
        thana.setDistrict(districtService.getRepository().getById(bean.getDistrictId()));
        return create(thana);
    }

    public Object deleteThana(String id) {
        Thana thana = getRepository().getById(id);
        thana.setStatus(Boolean.FALSE);
        update(id,thana);
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public Object updateThana(String id, ThanaCreateBean bean) {
        Thana thana = getRepository().getById(id);
        BeanUtils.copyProperties(bean,thana, AppConstant.IGNORE_PROPERTIES);
        return update(id,thana);
    }
}
