package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.SupportTicketResponseBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.issuetype.IssueTypeBean;
import com.ba.gas.monkey.dtos.reportissue.ReportIssueBean;
import com.ba.gas.monkey.dtos.reportissue.ReportIssueListBean;
import com.ba.gas.monkey.dtos.reportissue.CreateReportIssueRequestBean;
import com.ba.gas.monkey.dtos.reportissue.ReportIssueUpdateBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.IssueType;
import com.ba.gas.monkey.models.ReportIssue;
import com.ba.gas.monkey.repositories.ReportIssueRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.utility.AppUtilities;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("reportIssueService")
public class ReportIssueService extends BaseService<ReportIssue, ReportIssueBean> {

    private final CustomerService customerService;
    private final IssueTypeService issueTypeService;
    private final ReportIssueRepository reportIssueRepository;

    public ReportIssueService(BaseRepository<ReportIssue> repository, ModelMapper modelMapper,
                              CustomerService customerService, IssueTypeService issueTypeService,
                              ReportIssueRepository reportIssueRepository) {
        super(repository, modelMapper);
        this.customerService = customerService;
        this.issueTypeService = issueTypeService;
        this.reportIssueRepository = reportIssueRepository;
    }

    public ReportIssue createIssue(CreateReportIssueRequestBean bean) {
        Customer customer = customerService.getRepository().findById(bean.getCustomerId()).get();
        IssueType issueType = issueTypeService.getRepository().findById(bean.getIssueTypeId()).get();
        ReportIssue reportIssue = new ReportIssue();
        reportIssue.setIssueType(issueType);
        reportIssue.setCustomer(customer);
        reportIssue.setDetails(bean.getDetails());
        reportIssue.setTicketNo(AppConstant.SUPPORT_TICKET_PREFIX + AppUtilities.getNext());
        reportIssue.setUpdtId(AuthenticationUtils.getTokenUserId());
        reportIssue.setIssueStatus(0);
        reportIssue.setStatus(Boolean.TRUE);
        return getRepository().save(reportIssue);
    }

    public List<ReportIssue> getReportIssueList(String id) {
        return reportIssueRepository.findByCustomerId(id, Sort.by(Sort.Direction.DESC, "dateCreated"));
    }

    public Page<ReportIssueListBean> getReportIssues(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated"), Sort.Order.asc("issueStatus")));
        Page<ReportIssue> page = getRepository().findAll(pageRequest);
        List<ReportIssueListBean> list = page.getContent().stream().map(this::getListBean)
                .sorted(Comparator.comparing(ReportIssueListBean::getIssueStatus))
                .collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(list, page.getPageable(), page.getTotalElements());
    }

    private ReportIssueListBean getListBean(ReportIssue reportIssue) {
        ReportIssueListBean bean = getModelMapper().map(reportIssue, ReportIssueListBean.class);
        bean.setIssueType(getModelMapper().map(reportIssue.getIssueType(), IssueTypeBean.class));
        bean.setCustomer(getModelMapper().map(reportIssue.getCustomer(), CustomerBean.class));
        bean.setStatus(reportIssue.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        bean.setCreatedDate(reportIssue.getDateCreated());
        return bean;
    }

    public Object updateReportIssue(String id, ReportIssueUpdateBean issueBean) {
        ReportIssue reportIssue = getRepository().getById(id);
        reportIssue.setNote(issueBean.getNote());
        reportIssue.setIssueStatus(1);
        return update(id, reportIssue);
    }

    public SupportTicketResponseBean createSupportTicket(CreateReportIssueRequestBean bean) {
        ReportIssue reportIssue = createIssue(bean);
        return SupportTicketResponseBean.builder()
                .details(reportIssue.getDetails())
                .ticketNo(reportIssue.getTicketNo())
                .issueTypeId(reportIssue.getIssueType().getId())
                .customerId(reportIssue.getCustomer().getId())
                .build();
    }
}
