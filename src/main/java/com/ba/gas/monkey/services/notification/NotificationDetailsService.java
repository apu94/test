package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.notification.NotificationBean;
import com.ba.gas.monkey.dtos.notification.NotificationDetailsBean;
import com.ba.gas.monkey.dtos.notification.NotificationTypeBean;
import com.ba.gas.monkey.models.NotificationDetails;
import com.ba.gas.monkey.models.NotificationType;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service("notificationDetailsService")
public class NotificationDetailsService extends BaseService<NotificationDetails, NotificationDetailsBean> {
    public NotificationDetailsService(BaseRepository<NotificationDetails> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }
}
