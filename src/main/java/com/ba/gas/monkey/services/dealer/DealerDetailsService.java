package com.ba.gas.monkey.services.dealer;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.*;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.customer.CustomerInfoBean;
import com.ba.gas.monkey.dtos.customer.CustomerRegistrationBean;
import com.ba.gas.monkey.dtos.dealer.DealerCreateBean;
import com.ba.gas.monkey.dtos.dealer.DealerDetailsBean;
import com.ba.gas.monkey.dtos.dealer.DealerDetailsListBean;
import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.dtos.thana.ThanaBean;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.CustomerAttachmentRepository;
import com.ba.gas.monkey.repositories.CustomerRepository;
import com.ba.gas.monkey.repositories.DealerBrandRepository;
import com.ba.gas.monkey.repositories.DealerDetailsRepository;
import com.ba.gas.monkey.services.customer.CustomerAttachmentService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.customer.CustomerTypeService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import com.ba.gas.monkey.utility.PasswordUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("dealerDetailsService")
public class DealerDetailsService extends BaseService<DealerDetails, DealerDetailsBean> {

    private final UserInfoService userInfoService;
    private final CustomerTypeService customerTypeService;
    private final CustomerService customerService;
    private final CustomerAttachmentService customerAttachmentService;
    private final DealerBrandService dealerBrandService;
    private final PasswordUtils passwordUtils;

    public DealerDetailsService(BaseRepository<DealerDetails> repository, ModelMapper modelMapper,
                                UserInfoService userInfoService,
                                CustomerTypeService customerTypeService,
                                CustomerService customerService,
                                CustomerAttachmentService customerAttachmentService,
                                DealerBrandService dealerBrandService,
                                PasswordUtils passwordUtils) {
        super(repository, modelMapper);
        this.userInfoService = userInfoService;
        this.customerTypeService = customerTypeService;
        this.customerService = customerService;
        this.customerAttachmentService = customerAttachmentService;
        this.dealerBrandService = dealerBrandService;
        this.passwordUtils = passwordUtils;
    }

    public DealerDetails saveDealDetails(DealerDetailsBean dealerDetailsBean) {
        DealerDetails details = convertForCreate(dealerDetailsBean);
        details.setDateCreated(Instant.now());
        details.setDateModified(Instant.now());
        details.setUpdtId(userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId());
        return getRepository().save(details);
    }

    public Page<DealerDetailsListBean> getDealerListBeans(Pageable pageable) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.DEALER_TYPE);
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("approved"), Sort.Order.desc("dateCreated")));
        Page<Customer> page = ((CustomerRepository) customerService.getRepository()).findByCustomerType(customerType, pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getDealerDetailsListBean)
                .sorted(Comparator.comparing(DealerDetailsListBean::getStatus)).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private DealerDetailsListBean getDealerDetailsListBean(Customer customer) {
        DealerDetailsBean bean = getByCustomerId(customer.getId());
        bean.setCustomerInfoBean(getModelMapper().map(customer, CustomerInfoBean.class));
        DealerDetailsListBean listBean = getModelMapper().map(bean, DealerDetailsListBean.class);
        listBean.setStatus(customer.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        listBean.setApproved((bean.getCustomerInfoBean().getApproved()) ? AppConstant.APPROVED : AppConstant.NOT_APPROVED);
        listBean.setDepositBalance(0.0);
        return listBean;
    }

    private DealerDetailsBean getByCustomerId(String customerId) {
        DealerDetails details = ((DealerDetailsRepository) getRepository()).findByCustomerId(customerId);
        return getModelMapper().map(details, DealerDetailsBean.class);
    }

    public List<DealerDetailsBean> getNearestDealerList(DistanceBean distanceBean) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.DEALER_TYPE);
        distanceBean.setId(customerType.getId());
//        distanceBean.setDistanceInKM(AppConstant.DEFAULT_DISTANCE_FOR_SEARCH);
        List<Customer> list = customerService.getNearestDealerList(distanceBean);
        return list.stream().map(this::getDealerInfoBean).collect(Collectors.toUnmodifiableList());
    }

    private DealerDetailsBean getDealerInfoBean(Customer dealer) {
        DealerDetailsBean bean = getByCustomerId(dealer.getId());
        bean.setCustomerInfoBean(getModelMapper().map(dealer, CustomerInfoBean.class));
        bean.setBrands(dealerBrandService.getByDealerId(dealer.getId()));
        return bean;
    }

    public DealerDetailsBean profile() {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        DealerDetailsBean bean = getByCustomerId(customer.getId());
        bean.setCustomerInfoBean(getModelMapper().map(customer, CustomerInfoBean.class));
        return bean;
    }

    public DealerDetailsBean getDealerDetails(String id) {
        Customer customer = customerService.getRepository().getById(id);
        DealerDetailsBean bean = getByCustomerId(id);
        CustomerInfoBean infoBean = getModelMapper().map(customer, CustomerInfoBean.class);
        infoBean.setDistrict(getModelMapper().map(customer.getDistrict(), DistrictBean.class));
        infoBean.setThana(getModelMapper().map(customer.getThana(), ThanaBean.class));
        infoBean.setCluster(getModelMapper().map(customer.getCluster(), ClusterBean.class));
        bean.setCustomerInfoBean(infoBean);
        bean.setAttachmentBeans(customerAttachmentService.getAllAttachmentByCustomerId(id));
        bean.setBrands(dealerBrandService.getByDealerId(id));
        bean.setStatus(customer.getStatus());
        return bean;
    }

    public Object approvedDealer(String id) {
        Customer customer = customerService.getRepository().getById(id);
        customer.setApproved(Boolean.TRUE);
        customer.setUserStatus(AppConstant.ACTIVE.toLowerCase());
        customerService.update(id, customer);
        return getDealerDetails(id);
    }

    public Object profileUpdate(ProfileUpdateBean bean) {
        CustomerBean customerBean = customerService.profileUpdate(bean);
        return getDealerDetails(customerBean.getId());
    }

    public DealerDetailsBean saveDealer(DealerCreateBean bean) {
        CustomerType customerType = customerTypeService.getByCustomerType(AppConstant.DEALER_TYPE);
        bean.setCustomerTypeId(customerType.getId());
        String updateId = userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId();
        CustomerRegistrationBean customerRegistrationBean = getModelMapper().map(bean, CustomerRegistrationBean.class);
        customerRegistrationBean.setApproved(Boolean.TRUE);
        customerRegistrationBean.setUserStatus(AppConstant.ACTIVE.toLowerCase());
        customerRegistrationBean.setUpdtId(updateId);
        customerRegistrationBean.setPassword(passwordUtils.getDefaultPassword());
        CustomerInfoBean customerInfoBean = customerService.registration(customerRegistrationBean);
        DealerDetails details = getModelMapper().map(bean, DealerDetails.class);
        details.setCustomerId(customerInfoBean.getId());
        details.setDateCreated(Instant.now());
        details.setDateModified(Instant.now());
        details.setUpdtId(updateId);
        DealerDetails dealerDetails = getRepository().save(details);
        if (Objects.nonNull(bean.getBrandIds())) {
            dealerBrandService.createDealerBrand(convertForRead(dealerDetails), bean.getBrandIds());
        }
        if (Objects.nonNull(bean.getAttachments())) {
            customerAttachmentService.createCustomerAttachment(customerInfoBean.getId(), bean.getAttachments());
        }
        return getDealerDetails(customerInfoBean.getId());
    }

    public Object updateDealer(String id, DealerCreateBean bean) {
        CustomerBean customerBean = getModelMapper().map(bean, CustomerBean.class);
        customerService.updateCustomer(id, customerBean);
        DealerDetails dealerDetails = ((DealerDetailsRepository) getRepository()).findByCustomerId(id);
        DealerDetailsBean detailsBean = getModelMapper().map(bean, DealerDetailsBean.class);
        BeanUtils.copyProperties(detailsBean, dealerDetails, "id", "dateCreated");
        dealerDetails.setCustomerId(id);
        update(dealerDetails.getId(), dealerDetails);
        if (Objects.nonNull(bean.getBrandIds())) {
            dealerBrandService.updateDealerBrands(id, bean.getBrandIds());
        }
        if (Objects.nonNull(bean.getAttachments())) {
            customerAttachmentService.updateCustomerAttachment(id, bean.getAttachments());
        }
        return getDealerDetails(id);
    }

    @Override
    public DealerDetailsBean updateRowStatus(String id, Boolean status) {
        Customer customer = customerService.getRepository().getById(id);
        customer.setStatus(status);
        customerService.update(id, customer);
        return getDealerDetails(id);
    }

    public Object deletePartner() {
        String dealerId = Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId();
        Optional<DealerDetails> detailsOptional = ((DealerDetailsRepository) getRepository()).findByDealerId(dealerId);
        if (detailsOptional.isPresent()) {
            getRepository().delete(detailsOptional.get());
            Customer customer = customerService.getRepository().getById(dealerId);
            customer.setStatus(Boolean.FALSE);
            customerService.update(dealerId, customer);
            List<DealerBrand> dealerBrands = ((DealerBrandRepository) dealerBrandService.getRepository()).findByDealerId(dealerId);
            if (!dealerBrands.isEmpty()) {
                dealerBrandService.getRepository().deleteAll(dealerBrands);
            }
            List<CustomerAttachment> attachments = ((CustomerAttachmentRepository) customerAttachmentService.getRepository()).findByCustomerId(dealerId);
            if (!attachments.isEmpty()) {
                customerAttachmentService.getRepository().deleteAll(attachments);
            }
        }
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }
}
