package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.config.RabbitConfig;
import com.ba.gas.monkey.dtos.firebase.TransactionMaster;
import com.ba.gas.monkey.utility.Constant;
import org.springframework.stereotype.Service;

@Service
public class RabbitNotificationService {
    final private AMQPMessagePublisher amqpMessagePublisher;

    public RabbitNotificationService(AMQPMessagePublisher amqpMessagePublisher) {
        this.amqpMessagePublisher = amqpMessagePublisher;
    }

    public TransactionMaster sendNotification(TransactionMaster transactionMaster) {
        try {
            if(transactionMaster.getType().equalsIgnoreCase(Constant.ORDER)){
                amqpMessagePublisher.publishAMQPMessage(RabbitConfig.MESSAGE_QUEUE_ORDER, transactionMaster);
            }else if(transactionMaster.getType().equalsIgnoreCase(Constant.PAYMENT)){
                amqpMessagePublisher.publishAMQPMessage(RabbitConfig.MESSAGE_QUEUE_PAYMENT, transactionMaster);
            }else if(transactionMaster.getType().equalsIgnoreCase(Constant.PROMOTIONAL)){
                amqpMessagePublisher.publishAMQPMessage(RabbitConfig.MESSAGE_QUEUE_PROMOTIONAL, transactionMaster);
            }else if(transactionMaster.getType().equalsIgnoreCase(Constant.SCHEDULE)){
                amqpMessagePublisher.publishAMQPMessage(RabbitConfig.MESSAGE_QUEUE_SCHEDULE, transactionMaster);
            }else if(transactionMaster.getType().equalsIgnoreCase(Constant.SUPPORT)){
                amqpMessagePublisher.publishAMQPMessage(RabbitConfig.MESSAGE_QUEUE_SUPPORT, transactionMaster);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Inside exception:"+e.getMessage());
        }
        System.out.println("before return:");
        return transactionMaster;
    }
}
