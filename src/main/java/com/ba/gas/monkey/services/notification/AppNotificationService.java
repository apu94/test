package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.dtos.DeviceTokenBean;
import com.ba.gas.monkey.dtos.firebase.Note;
import com.ba.gas.monkey.dtos.firebase.TransactionMaster;
import com.ba.gas.monkey.dtos.partner.PartnerDetailsBean;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.NotificationTypeRepository;
import com.ba.gas.monkey.repositories.TemplateRepository;
import com.ba.gas.monkey.services.DeviceTokenService;
import com.ba.gas.monkey.services.TemplateService;
import com.ba.gas.monkey.services.firebase.FirebaseMessagingService;
import com.ba.gas.monkey.utility.Constant;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service("appNotificationService")
public class AppNotificationService {

    private final FirebaseMessagingService firebaseMessagingService;
    private final DeviceTokenService deviceTokenService;
    private final NotificationService notificationService;
    private final RabbitNotificationService rabbitNotificationService;
    private final TemplateService templateService;
    private final NotificationDetailsService notificationDetailsService;
    private final NotificationTypeService notificationTypeService;

    public AppNotificationService(FirebaseMessagingService firebaseMessagingService,
                                  DeviceTokenService deviceTokenService, NotificationService notificationService,
                                  RabbitNotificationService rabbitNotificationService,
                                  TemplateService templateService,
                                  NotificationDetailsService notificationDetailsService,
                                  NotificationTypeService notificationTypeService) {
        this.firebaseMessagingService = firebaseMessagingService;
        this.deviceTokenService = deviceTokenService;
        this.notificationService = notificationService;
        this.rabbitNotificationService = rabbitNotificationService;
        this.templateService = templateService;
        this.notificationDetailsService = notificationDetailsService;
        this.notificationTypeService = notificationTypeService;
    }

    public void customerOrderConfirmation(OrderInfo orderInfo) {
        System.out.println("customer order notification:");
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.CUSTOMER_ORDER_CONFIRMATION).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getCustomer().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getCustomer().getId());
            System.out.println("just before customer order notification:");
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            System.out.println("just before customer order notification:");
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
    }

    public void sendOrderActionNotification(OrderInfo orderInfo, PartnerDetailsBean partnerDetailsBean) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.SEND_ORDER_ACTION).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(partnerDetailsBean.getCustomerInfoBean().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), partnerDetailsBean.getCustomerInfoBean().getId());
            Note note = generateNote(orderInfo, "action", template, Constant.PARTNER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });


//        Note note = new Note();
//        note.setSubject("New Order.");
//        note.setContent("You have an order!");
//        Map<String, String> dataValue = new HashMap<>();
//        dataValue.put("orderId", orderInfo.getId());
//        dataValue.put("type", "action");
//        note.setDataValue(dataValue);
//        deviceTokenService.getDeviceTokenByUserId(partnerDetailsBean.getCustomerInfoBean().getId()).forEach(token -> {
//            try {
//                System.out.println("partner id:" + partnerDetailsBean.getCustomerInfoBean().getId());
//                System.out.println("token:" + token.getToken());
//                firebaseMessagingService.sendNotification(note, token.getToken());
//            } catch (FirebaseMessagingException e) {
//                e.printStackTrace();
//            }
//        });
    }

    boolean sendNotification = false;

    public String adminSendOrderActionNotification(String orderId, String partnerId) {
        Note note = new Note();
        note.setType(Constant.PARTNER);
        note.setSubject("New Order.");
        note.setContent("You have an order!");
        Map<String, String> dataValue = new HashMap<>();
        dataValue.put("orderId", orderId);
        dataValue.put("type", "action");
        note.setDataValue(dataValue);
        deviceTokenService.getDeviceTokenByUserId(partnerId).forEach(token -> {
            try {
                System.out.println("partner id:" + partnerId);
                System.out.println("token:" + token.getToken());
                System.out.println("device type:" + token.getDeviceType());
                if (token.getDeviceType() != null && token.getDeviceType().equalsIgnoreCase(Constant.DEVICE_TYPE_ANDROID)) {
                    firebaseMessagingService.sendNotificationAndroid(note, token.getToken());
                } else {
                    firebaseMessagingService.sendNotification(note, token.getToken());
                }
                sendNotification = true;
            } catch (FirebaseMessagingException e) {
                e.printStackTrace();
                sendNotification = false;
            }
        });
        return sendNotification ? "Success" : "Fail";
    }

    public void customerOrderPicked(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.CUSTOMER_ORDER_PICKED).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getCustomer().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getCustomer().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
//        Note note = new Note();
//        note.setSubject("Order Picked.");
//        note.setContent("Dear Sir, Your Order Has Been Picked for delivery.");
//        Map<String, String> dataValue = new HashMap<>();
//        dataValue.put("orderId", orderInfo.getId());
//        dataValue.put("type", "general");
//        note.setDataValue(dataValue);
//        deviceTokenService.getDeviceTokenByUserId(orderInfo.getCustomer().getId()).forEach(token -> {
//            try {
//                firebaseMessagingService.sendNotification(note, token.getToken());
//            } catch (FirebaseMessagingException e) {
//                e.printStackTrace();
//            }
//        });
    }

    public void customerOrderDelivered(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.CUSTOMER_ORDER_DELIVERED).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getCustomer().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getCustomer().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
//        Note note = new Note();
//        note.setSubject("Delivery Complete.");
//        note.setContent("Dear Sir, Your Order No " + orderInfo.getOrderNumber() + " has been delivered successfully.");
//        Map<String, String> dataValue = new HashMap<>();
//        dataValue.put("orderId", orderInfo.getId());
//        dataValue.put("type", "general");
//        note.setDataValue(dataValue);
//        deviceTokenService.getDeviceTokenByUserId(orderInfo.getCustomer().getId()).forEach(token -> {
//            try {
//                firebaseMessagingService.sendNotification(note, token.getToken());
//            } catch (FirebaseMessagingException e) {
//                e.printStackTrace();
//            }
//        });
    }

    public void customerOrderCanceled(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.CUSTOMER_ORDER_CANCELED).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getCustomer().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getCustomer().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
    }

    public void partnerOrderReceived(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.PARTNER_ORDER_RECEIVED).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getPartner().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getPartner().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });

//        Note note = new Note();
//        note.setSubject("Order Received.");
//        note.setContent("Delivery Completed!");
//        Map<String, String> dataValue = new HashMap<>();
//        dataValue.put("orderId", orderInfo.getId());
//        dataValue.put("type", "general");
//        note.setDataValue(dataValue);
//        deviceTokenService.getDeviceTokenByUserId(orderInfo.getPartner().getId()).forEach(token -> {
//            try {
//                firebaseMessagingService.sendNotification(note, token.getToken());
//            } catch (FirebaseMessagingException e) {
//                e.printStackTrace();
//            }
//        });
    }

    public void partnerOrderCanceled(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.PARTNER_ORDER_CANCELED).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getPartner().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getPartner().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
    }

    public void sendPartnerNotFoundNotification(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.ADMIN_PARTNER_NOT_FOUND).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getAllAdminDeviceToken();
        log.info("get-admin-users {}", tokenBeans.stream().map(DeviceTokenBean::getUserId).collect(Collectors.toUnmodifiableList()));
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "ADMIN", "ADMIN", token.getToken(), notificationType.getId(), token.getUserId());
            Note note = generateNote(orderInfo, "general", template, Constant.ADMIN);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
    }


    public void dealerOrderPicked(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.DEALER_ORDER_PICKED).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getDealer().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getDealer().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
//        Note note = new Note();
//        note.setSubject("Order Picked.");
//        note.setContent("You have an order. Our Partner is on the way.");
//        Map<String, String> dataValue = new HashMap<>();
//        dataValue.put("orderId", orderInfo.getId());
//        dataValue.put("type", "general");
//        note.setDataValue(dataValue);
//        deviceTokenService.getDeviceTokenByUserId(orderInfo.getDealer().getId()).forEach(token -> {
//            try {
//                firebaseMessagingService.sendNotification(note, token.getToken());
//            } catch (FirebaseMessagingException e) {
//                e.printStackTrace();
//            }
//        });
    }

    public void dealerOrderEmptyCylinder(OrderInfo orderInfo) {
        Template template = ((TemplateRepository) templateService.getRepository()).findByAppToUser(Constant.DEALER_EMPTY_CYLINDER).get();
        NotificationType notificationType = ((NotificationTypeRepository) notificationTypeService.getRepository()).findByName(Constant.ORDER).get();
        List<DeviceTokenBean> tokenBeans = deviceTokenService.getDeviceTokenByUserId(orderInfo.getDealer().getId());
        tokenBeans.forEach(token -> {
            Notification notificationBean = getOrderNotification(orderInfo, template, "APP", token.getDeviceType(), token.getToken(), notificationType.getId(), orderInfo.getDealer().getId());
            Note note = generateNote(orderInfo, "general", template, Constant.CUSTOMER);
            rabbitNotificationService.sendNotification(generateTransactionMaster(note, notificationBean.getId(), token.getToken(), Constant.ORDER, token.getDeviceType()));
        });
//        Note note = new Note();
//        note.setSubject("Empty Cylinder return");
//        note.setContent("Your empty Cylinder Has been Returned.");
//        Map<String, String> dataValue = new HashMap<>();
//        dataValue.put("orderId", orderInfo.getId());
//        dataValue.put("type", "general");
//        note.setDataValue(dataValue);
//        deviceTokenService.getDeviceTokenByUserId(orderInfo.getDealer().getId()).forEach(token -> {
//            try {
//                firebaseMessagingService.sendNotification(note, token.getToken());
//            } catch (FirebaseMessagingException e) {
//                e.printStackTrace();
//            }
//        });
    }

    public Notification getOrderNotification(OrderInfo orderInfo, Template template, String module, String deviceType, String token, String notificationTypeId, String receiverId) {
        Notification notification = new Notification();
        notification.setNotificationStatus(0);
        notification.setNotificationType(notificationTypeId);
        notification.setMediaType("PUSH");
        notification.setNoOfTry(1);
        notification.setReceiverId(receiverId);
        notification.setModule(module);
        notification.setTemplate(template);
        notification.setUpdtId(orderInfo.getCustomer().getId());
        notification.setDeviceType(deviceType);
        notification.setDeviceToken(token);
        notification.setDateCreated(Instant.now());
        notification.setDateModified(Instant.now());
        notification.setStatus(Boolean.TRUE);
        notification.setOrderId(orderInfo.getId());
        notification.setSeen(false);
        notification = notificationService.getRepository().save(notification);
        NotificationDetails notificationDetails = new NotificationDetails();
        notificationDetails.setNotification(notification);
        notificationDetails.setSubject(template.getTitle());
        notificationDetails.setContent(template.getDescription().replace("$order_number", "" + orderInfo.getOrderNumber()));
        notificationDetails.setUpdtId(orderInfo.getCustomer().getId());
        notificationDetails.setDateCreated(Instant.now());
        notificationDetails.setDateModified(Instant.now());
        notificationDetails.setStatus(Boolean.TRUE);
        notificationDetailsService.getRepository().save(notificationDetails);
        return notification;
    }

    public Note generateNote(OrderInfo orderInfo, String type, Template template, String sendTo) {
        Note note = new Note();
        note.setType(sendTo);
        note.setSubject(template.getTitle());
        note.setContent(template.getDescription().replace("$order_number", "" + orderInfo.getOrderNumber()));
        Map<String, String> dataValue = new HashMap<>();
        dataValue.put("orderId", orderInfo.getId());
        dataValue.put("type", type);
        note.setDataValue(dataValue);
        return note;
    }

    public Note generatePromotionalNotificationNote(Notification notification, String type) {
        Note note = new Note();
        note.setSubject(notification.getNotificationDetails().getSubject());
        note.setContent(notification.getNotificationDetails().getContent());
        Map<String, String> dataValue = new HashMap<>();
        dataValue.put("orderId", "");
        dataValue.put("type", type);
        note.setDataValue(dataValue);
        return note;
    }

    public TransactionMaster generateTransactionMaster(Note note, String notificationId, String token, String type, String deviceType) {
        TransactionMaster transactionMaster = new TransactionMaster();
        transactionMaster.setNote(note);
        transactionMaster.setNotificationId(notificationId);
        transactionMaster.setToken(token);
        transactionMaster.setDeviceType(deviceType);
        transactionMaster.setType(type);
        return transactionMaster;
    }

    public void sendPromotionalNotification(Notification notification, Note note) {
        rabbitNotificationService.sendNotification(generateTransactionMaster(note, notification.getId(), notification.getDeviceToken(), Constant.PROMOTIONAL, notification.getDeviceType()));
    }
}
