package com.ba.gas.monkey.services.designation;


import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.department.DepartmentBean;
import com.ba.gas.monkey.dtos.department.DepartmentListBean;
import com.ba.gas.monkey.dtos.designation.DesignationBean;
import com.ba.gas.monkey.dtos.designation.DesignationListBean;
import com.ba.gas.monkey.models.Department;
import com.ba.gas.monkey.models.Designation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("designationService")
public class DesignationService extends BaseService<Designation, DesignationBean> {

    public DesignationService(BaseRepository<Designation> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        List<Designation> list = getRepository().findAll(Sort.by("name")).stream().filter(Designation::getStatus).collect(Collectors.toUnmodifiableList());
        return list.stream().map(this::getDropdownDto).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDto(Designation designation) {
        DropdownDTO dropdownDTO = getModelMapper().map(designation, DropdownDTO.class);
        dropdownDTO.setValue(designation.getName());
        return dropdownDTO;
    }

    public Object createDesignation(DesignationBean bean) {
        return create(convertForCreate(bean));
    }

    public Object updateDesignation(String id, DesignationBean bean) {
        Designation designation = getRepository().getById(id);
        BeanUtils.copyProperties(bean, designation, AppConstant.IGNORE_PROPERTIES);
        return update(id, designation);
    }

    public Object getDesignationList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("name"));
        Page<Designation> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean)
                .sorted(Comparator.comparing(DesignationListBean::getStatus))
                .collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private DesignationListBean getListBean(Designation designation) {
        DesignationListBean bean = getModelMapper().map(designation, DesignationListBean.class);
        bean.setStatus(designation.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }
}
