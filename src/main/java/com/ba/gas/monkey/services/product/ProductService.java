package com.ba.gas.monkey.services.product;

import com.ba.gas.monkey.base.BaseEntity;
import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.BrandReviewInfoBean;
import com.ba.gas.monkey.dtos.brand.PopularBrandBean;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.product.*;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.BrandRepository;
import com.ba.gas.monkey.repositories.FavouriteBrandRepository;
import com.ba.gas.monkey.repositories.OrderProductRepository;
import com.ba.gas.monkey.repositories.ProductRepository;
import com.ba.gas.monkey.services.brand.BrandReviewService;
import com.ba.gas.monkey.services.brand.BrandService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("productService")
public class ProductService extends BaseService<Product, ProductBean> {

    private final BrandService brandService;
    private final BrandReviewService brandReviewService;
    private final ProductSizeService productSizeService;
    private final ProductValveSizeService productValveSizeService;
    private final ProductPriceService productPriceService;
    private final ProductRepository productRepository;
    private final FavouriteBrandRepository favouriteBrandRepository;
    private final OrderProductRepository orderProductRepository;


    public ProductService(BaseRepository<Product> repository, ModelMapper modelMapper,
                          BrandService brandService, ProductSizeService productSizeService,
                          ProductValveSizeService productValveSizeService,
                          ProductPriceService productPriceService,
                          ProductRepository productRepository,
                          FavouriteBrandRepository favouriteBrandRepository,
                          OrderProductRepository orderProductRepository,
                          BrandReviewService brandReviewService) {
        super(repository, modelMapper);
        this.brandService = brandService;
        this.productSizeService = productSizeService;
        this.productValveSizeService = productValveSizeService;
        this.productPriceService = productPriceService;
        this.productRepository = productRepository;
        this.favouriteBrandRepository = favouriteBrandRepository;
        this.brandReviewService = brandReviewService;
        this.orderProductRepository = orderProductRepository;
    }

    public Page<ProductListBean> getProductList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("status"), Sort.Order.desc("dateModified")));
        Page<Product> page = getRepository().findAll(pageRequest);
        List<ProductListBean> beans = page.getContent().stream().map(this::getProductListBean).collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(beans, pageable, page.getTotalElements());
    }

    public List<ProductDetailsBean> getFeatureProductList() {
        List<Product> products = getRepository().findAll().stream().filter(Product::getStatus).filter(l -> Objects.nonNull(l.getFeatureProduct()) && l.getFeatureProduct()).collect(Collectors.toUnmodifiableList());
        return products.stream().map(this::getProductDetailsBean)
                .sorted(Comparator.comparing(ProductDetailsBean::getNameEn))
                .collect(Collectors.toUnmodifiableList());
    }

    /*all-products*/
    public List<ProductDetailsBean> getAllProductList() {
        return getRepository().findAll().stream().filter(Product::getStatus)
                .map(this::getProductDetailsBean)
                .sorted(Comparator.comparing(l -> l.getBrand().getNameEn()))
                .collect(Collectors.toUnmodifiableList());
    }

    private ProductDetailsBean getProductDetailsBean(Product product) {
        ProductDetailsBean detailsBean = getModelMapper().map(product, ProductDetailsBean.class);
        List<BrandReviewInfoBean> brandReviewList = brandReviewService.brandReviewList(detailsBean.getBrand().getId());
        List<Double> ratings = brandReviewList.stream().map(BrandReviewInfoBean::getRatingPoint).collect(Collectors.toUnmodifiableList());
        OptionalDouble average = ratings.stream().mapToDouble(a -> a).average();
        detailsBean.setRating(String.valueOf(average.orElse(0.0)));
        detailsBean.setReview(brandReviewList.size() + " reviews");
        detailsBean.setRatingInt((int) average.orElse(0.0));
        detailsBean.getBrand().setDiscountEnabled(checkOfferValidation(product.getBrand()));
        return detailsBean;
    }


    public ProductDetailsBean getProductById(String id) {
        Product product = getById(id);
        List<BrandReviewInfoBean> brandReviewList = brandReviewService.brandReviewList(product.getBrand().getId());
        List<Double> ratings = brandReviewList.stream().map(BrandReviewInfoBean::getRatingPoint).collect(Collectors.toUnmodifiableList());
        OptionalDouble average = ratings.stream().mapToDouble(a -> a).average();
        ProductDetailsBean detailsBean = getModelMapper().map(product, ProductDetailsBean.class);
        detailsBean.setRating(String.valueOf(average.orElse(0.0)));
        detailsBean.setReview(brandReviewList.size() + " reviews");
        detailsBean.setProductImageList(product.getProductImageList().stream().map(this::getImageListBean).collect(Collectors.toUnmodifiableList()));
        detailsBean.setReviewList(brandReviewService.brandReviewList(product.getBrand().getId()));
        detailsBean.getBrand().setDiscountEnabled(checkOfferValidation(product.getBrand()));
        return detailsBean;
    }

    public ProductDetailsBean getProductDetailsFilter(ProductDetailFilterBean productDetailFilterBean) {
        List<Product> productList = productRepository.findByBrandAndProductSizeAndProductValveSize(brandService.getRepository().getById(productDetailFilterBean.getBrandId()), productSizeService.getRepository().getById(productDetailFilterBean.getProductSizeId()), productValveSizeService.getRepository().getById(productDetailFilterBean.getProductValveSizeId()));
        if (productList.isEmpty()) {
            return new ProductDetailsBean();
        } else {
            return getProductDetailsBean(productList.get(0));
        }
    }


    public ProductBean createProduct(ProductCreateBean productCreateBean) {
        Product product = getModelMapper().map(productCreateBean, Product.class);
        product.setBrand(brandService.getRepository().getById(productCreateBean.getBrandId()));
        product.setProductSize(productSizeService.getRepository().getById(productCreateBean.getProductSizeId()));
        product.setProductValveSize(productValveSizeService.getRepository().getById(productCreateBean.getProductValveSizeId()));
        return create(product);
    }

    public ProductDetailsBean updateProduct(String id, ProductEditBean productEditBean) {
        Product product = getById(id);
        BeanUtils.copyProperties(productEditBean, product);
        product.setBrand(brandService.getRepository().getById(productEditBean.getBrandId()));
        product.setProductSize(productSizeService.getRepository().getById(productEditBean.getProductSizeId()));
        product.setProductValveSize(productValveSizeService.getRepository().getById(productEditBean.getProductValveSizeId()));
        ProductBean updatedProduct = update(product.getId(), product);
        productPriceService.updateProductPrice(product, productEditBean.getProductPriceBean());
        return getProductById(updatedProduct.getId());
    }

    private ProductListBean getProductListBean(Product product) {
        Double refilPrice = null;
        Double emptyCylinerPrice = null;
        Double packagePrice = null;
        if (Objects.nonNull(product.getProductPrice())) {
            refilPrice = product.getProductPrice().getRefillPrice();
            emptyCylinerPrice = product.getProductPrice().getEmptyCylinderPrice();
            packagePrice = product.getProductPrice().getPackagePrice();
        }
        List<ProductImage> productImages = product.getProductImageList().stream().filter(ProductImage::getStatus).collect(Collectors.toUnmodifiableList());
        String productPhoto = (productImages.isEmpty()) ? "" : productImages.get(0).getImageLink();
        return ProductListBean.builder().id(product.getId())
                .brandNameEn(product.getBrand().getNameEn())
                .brandNameBn(product.getBrand().getNameBn())
                .companyName(product.getBrand().getCompanyName())
                .productSizeEn(product.getProductSize().getNameEn())
                .productSizeBn(product.getProductSize().getNameBn())
                .valveSizeEn(product.getProductValveSize().getNameEn())
                .valveSizeBn(product.getProductValveSize().getNameBn())
                .refillPrice(refilPrice).emptyCylinderPrice(emptyCylinerPrice)
                .packagePrice(packagePrice).productPhoto(productPhoto)
                .productCode(product.getCode())
                .featureProduct(product.getFeatureProduct() ? AppConstant.STATUS_YES : AppConstant.STATUS_NO)
                .offerProduct(product.getOfferProduct() ? AppConstant.STATUS_YES : AppConstant.STATUS_NO)
                .status(product.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE).build();
    }


    private PopularBrandBean getFeatureProductListBean(Product product) {

        String productPhoto = null;
        if (!product.getProductImageList().isEmpty()) {
            productPhoto = product.getProductImageList().get(0).getImageLink();
        }
        return PopularBrandBean.builder().id(product.getId()).brandNameEn(product.getBrand().getNameEn()).brandNameBn(product.getBrand().getNameBn()).rating("1.0").review("10 reviews").imageUrl(productPhoto).favoriteStatus(false).build();
    }

    private ProductImageListBean getImageListBean(ProductImage productImage) {
        ProductImageListBean bean = getModelMapper().map(productImage, ProductImageListBean.class);
        bean.setStatus(productImage.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }


    public List<ProductDetailsBean> getProductFilter(ProductFilterBean productFilterBean) {
        if (Objects.isNull(productFilterBean.getBrandIds()) && Objects.isNull(productFilterBean.getProductSizeIds()) && Objects.isNull(productFilterBean.getProductValveSizeIds())) {
            return new ArrayList<>();
        }
        List<Product> productList = new ArrayList<>(getRepository().findAll().stream().filter(Product::getStatus).collect(Collectors.toUnmodifiableList()));

        if (Objects.nonNull(productFilterBean.getBrandIds()) && !productFilterBean.getBrandIds().isEmpty()) {
            List<String> brandIds = productFilterBean.getBrandIds();
            List<Product> collect = productList.stream().filter(l -> !brandIds.contains(l.getBrand().getId())).collect(Collectors.toUnmodifiableList());
            productList.removeAll(collect);
        }

        if (Objects.nonNull(productFilterBean.getProductSizeIds()) && !productFilterBean.getProductSizeIds().isEmpty()) {
            List<String> productSizeIds = productFilterBean.getProductSizeIds();
            List<Product> collect = productList.stream().filter(l -> !productSizeIds.contains(l.getProductSize().getId())).collect(Collectors.toUnmodifiableList());
            productList.removeAll(collect);
        }

        if (Objects.nonNull(productFilterBean.getProductValveSizeIds()) && !productFilterBean.getProductValveSizeIds().isEmpty()) {
            List<String> valveSizeIds = productFilterBean.getProductValveSizeIds();
            List<Product> collect = productList.stream().filter(l -> !valveSizeIds.contains(l.getProductValveSize().getId())).collect(Collectors.toUnmodifiableList());
            productList.removeAll(collect);
        }

        List<ProductDetailsBean> beans = new ArrayList<>(productList.stream().sorted(Comparator.comparing(l -> l.getBrand().getId())).map(this::getProductDetailsBean).collect(Collectors.toUnmodifiableList()));
        if (Objects.nonNull(productFilterBean.getRatings()) && !productFilterBean.getRatings().isEmpty()) {
            List<Integer> integers = productFilterBean.getRatings().stream().map(Integer::parseInt).collect(Collectors.toList());
            List<ProductDetailsBean> detailsBeans = beans.stream().filter(l -> !integers.contains(l.getRatingInt())).collect(Collectors.toUnmodifiableList());
            beans.removeAll(detailsBeans);
        }
        return beans;
    }


    public List<Product> getByBrand(Brand brand) {
        return ((ProductRepository) getRepository()).findAllByBrandIds(Collections.singletonList(brand.getId()));
    }

    public List<PopularBrandBean> getPopularBrandList() {
        List<PopularBrandBean> list = new ArrayList<>();
        List<Brand> brands = brandService.getRepository().findAll().stream().filter(Brand::getStatus).collect(Collectors.toUnmodifiableList());
        List<String> brandIds = getFavouriteBrandByCustomer();
        brands.forEach(b -> {
            PopularBrandBean brandBean = getPopularBrandBean(b, brandIds.contains(b.getId()));
            if (Objects.nonNull(brandBean)) {
                list.add(brandBean);
            }
        });
        return list.stream().filter(f -> Objects.nonNull(f.getProduct()))
                .sorted(Comparator.comparing(PopularBrandBean::getBrandNameEn))
                .collect(Collectors.toUnmodifiableList());
    }

    private PopularBrandBean getPopularBrandListBean(Brand brand) {
        List<Product> products = getByBrand(brand);
        List<BrandReviewInfoBean> brandReviewList = brandReviewService.brandReviewList(brand.getId());
        List<Double> ratings = brandReviewList.stream().map(BrandReviewInfoBean::getRatingPoint).collect(Collectors.toUnmodifiableList());
        OptionalDouble average = ratings.stream().mapToDouble(a -> a).average();
        String brandImage = (!brand.getBrandImageList().isEmpty()) ? brand.getBrandImageList().get(0).getImageLink() : null;
        return PopularBrandBean.builder().id(brand.getId()).brandNameEn(brand.getNameEn())
                .brandNameBn(brand.getNameBn()).rating(String.valueOf(average.orElse(0.0)))
                .review(brandReviewList.size() + " reviews").imageUrl(brandImage)
                .favoriteStatus(false).product(!products.isEmpty() ? products.get(0) : null).build();
    }

    public List<PopularBrandBean> getOfferedBrandList() {
        List<PopularBrandBean> list = new ArrayList<>();
        List<Brand> offerBrands = brandService.getRepository().findAll()
                .stream()
                .filter(BaseEntity::getStatus)
                .filter(this::checkOfferValidation)
                .collect(Collectors.toUnmodifiableList());
        List<String> brandIds = getFavouriteBrandByCustomer();
        offerBrands.forEach(brand -> list.add(getPopularBrandBean(brand, brandIds.contains(brand.getId()))));
        return list.stream().sorted(Comparator.comparing(PopularBrandBean::getBrandNameEn)).collect(Collectors.toUnmodifiableList());
    }

    private boolean checkOfferValidation(Brand brand) {
        if (Objects.nonNull(brand.getDiscountEnabled()) && Objects.nonNull(brand.getDiscountEndDate()) && brand.getDiscountEnabled()) {
            return brand.getDiscountEndDate().isAfter(LocalDate.now());
        }
        return Boolean.FALSE;
    }

    private PopularBrandBean getPopularBrandBean(Brand brand, Boolean status) {
        List<Product> products = getByBrand(brand).stream().filter(f -> Objects.nonNull(f.getFeatureProduct()) && f.getFeatureProduct()).collect(Collectors.toUnmodifiableList());
        if (products.isEmpty()) {
            return null;
        }
        List<BrandReviewInfoBean> brandReviewList = brandReviewService.brandReviewList(brand.getId());
        List<Double> ratings = brandReviewList.stream().map(BrandReviewInfoBean::getRatingPoint).collect(Collectors.toUnmodifiableList());
        OptionalDouble average = ratings.stream().mapToDouble(a -> a).average();
        String brandImage = (!brand.getBrandImageList().isEmpty()) ? brand.getBrandImageList().get(0).getImageLink() : null;
        return PopularBrandBean.builder().id(brand.getId()).brandNameEn(brand.getNameEn()).brandNameBn(brand.getNameBn())
                .rating(String.valueOf(average.orElse(0.0))).review(brandReviewList.size() + " reviews")
                .imageUrl(brandImage).favoriteStatus(status).product(!products.isEmpty() ? products.get(0) : null).build();
    }

    public List<PopularBrandBean> getAllBrandList() {
        List<PopularBrandBean> list = new ArrayList<>();
        List<Brand> brands = brandService.getRepository().findAll().stream().filter(Brand::getStatus).collect(Collectors.toUnmodifiableList());
        List<String> brandIds = getFavouriteBrandByCustomer();
        brands.forEach(brand -> list.add(getPopularBrandBean(brand, brandIds.contains(brand.getId()))));
        return list.stream().filter(f -> Objects.nonNull(f.getProduct()))
                .sorted(Comparator.comparing(PopularBrandBean::getBrandNameEn))
                .collect(Collectors.toUnmodifiableList());
    }

    public List<ProductDetailsBean> searchBrand(String searchBrand) {
        if (searchBrand.length() < 2) {
            throw new ServiceExceptionHolder.CustomException(1002, "Search text minimum 2 character");
        }
        List<String> brandIds = ((BrandRepository) brandService.getRepository())
                .search(searchBrand).stream()
                .map(Brand::getId)
                .collect(Collectors.toUnmodifiableList());
        return ((ProductRepository) getRepository()).findAllByBrandIds(brandIds)
                .stream().filter(BaseEntity::getStatus).map(this::getProductDetailsBean)
                .sorted(Comparator.comparing(detailsBean -> detailsBean.getBrand().getNameEn()))
                .collect(Collectors.toUnmodifiableList());
    }

    public List<PopularBrandBean> favouriteBrandByCustomer() {
        List<PopularBrandBean> list = new ArrayList<>();
        List<Brand> brands = favouriteBrandRepository.findByCustomerId(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId())
                .stream()
                .filter(FavouriteBrand::getStatus)
                .map(FavouriteBrand::getBrand).collect(Collectors.toUnmodifiableList());
        brands.forEach(brand -> list.add(getPopularBrandBean(brand, Boolean.TRUE)));
        return list;
    }

    private List<String> getFavouriteBrandByCustomer() {
        List<String> list = new ArrayList<>();
        CustomerBean customerBean = AuthenticationUtils.getCustomerDetails();
        if (Objects.nonNull(customerBean)) {
            list.addAll(favouriteBrandRepository.findByCustomerId(customerBean.getId())
                    .stream()
                    .filter(FavouriteBrand::getStatus)
                    .map(FavouriteBrand::getBrand)
                    .collect(Collectors.toUnmodifiableList())
                    .stream().map(Brand::getId)
                    .collect(Collectors.toUnmodifiableList()));
        }
        return list;
    }

    public ProductDetailsBean productCompare(ProductCompareBean productCompareBean) {
        List<Product> list = productRepository.findByBrandAndProductSizeAndProductValveSize(brandService.getRepository().getById(productCompareBean.getBrandId()),
                productSizeService.getRepository().getById(productCompareBean.getProductSizeId()),
                productValveSizeService.getRepository().getById(productCompareBean.getProductValveSizeId()));
        List<ProductDetailsBean> detailsBeans = list.stream().map(this::getProductDetailsBean).collect(Collectors.toUnmodifiableList());
        return !detailsBeans.isEmpty() ? detailsBeans.get(0) : null;
    }

    public Object productListForWeb() {
        List<ProductDetailsBean> allProductList = getAllProductList();
        return allProductList.stream().map(this::getBasicInfoBean).collect(Collectors.toUnmodifiableList());
    }

    private ProductBasicInfoBean getBasicInfoBean(ProductDetailsBean detailsBean) {
        ProductBasicInfoBean bean = getModelMapper().map(detailsBean, ProductBasicInfoBean.class);
        bean.setBrandName(detailsBean.getBrand().getNameEn());
        bean.setImage(detailsBean.getProductImageList().isEmpty() ? AppConstant.EMPTY_STRING : detailsBean.getProductImageList().get(0).getImageLink());
        bean.setValveSize(detailsBean.getProductValveSize());
        return bean;
    }

    public Object getPopularProductList() {
        return getRepository().findAll().stream()
                .filter(BaseEntity::getStatus)
                .filter(f -> Objects.nonNull(f.getFeatureProduct()) && f.getFeatureProduct())
                .map(this::getProductDetailsBean)
                .sorted(Comparator.comparing(detailsBean -> detailsBean.getBrand().getNameEn()))
                .collect(Collectors.toUnmodifiableList());
//        return orderProductRepository.findAllDistinctBuyProducts()
//                .stream().filter(BaseEntity::getStatus).map(this::getProductDetailsBean)
//                .sorted(Comparator.comparing(detailsBean -> detailsBean.getBrand().getNameEn()))
//                .collect(Collectors.toUnmodifiableList());
    }

    public Object getOfferedProductList() {
        List<Brand> allBrands = brandService.getRepository().findAll().stream().filter(f -> Objects.nonNull(f.getDiscountEnabled())).collect(Collectors.toUnmodifiableList());
        List<String> brandIds = allBrands.stream().filter(Brand::getStatus).filter(this::checkOfferValidation).map(Brand::getId).collect(Collectors.toUnmodifiableList());
        return ((ProductRepository) getRepository()).findAllByBrandIds(brandIds)
                .stream().filter(BaseEntity::getStatus).map(this::getProductDetailsBean)
                .sorted(Comparator.comparing(detailsBean -> detailsBean.getBrand().getNameEn()))
                .collect(Collectors.toUnmodifiableList());

    }
}
