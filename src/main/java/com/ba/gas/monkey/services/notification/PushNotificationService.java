package com.ba.gas.monkey.services.notification;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.pushnotification.PushNotificationBean;
import com.ba.gas.monkey.dtos.pushnotification.PushNotificationListBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.PushNotification;
import com.ba.gas.monkey.repositories.CustomerRepository;
import com.ba.gas.monkey.repositories.CustomerTypeRepository;
import com.ba.gas.monkey.repositories.TemplateRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service("pushNotificationService")
public class PushNotificationService extends BaseService<PushNotification, PushNotificationBean> {

    private final NotificationService notificationService;
    private final CustomerTypeRepository customerTypeRepository;
    private final TemplateRepository templateRepository;

    public PushNotificationService(BaseRepository<PushNotification> repository,
                                   ModelMapper modelMapper,
                                   NotificationService notificationService,
                                   CustomerTypeRepository customerTypeRepository,
                                   TemplateRepository templateRepository) {
        super(repository, modelMapper);
        this.customerTypeRepository = customerTypeRepository;
        this.templateRepository = templateRepository;
        this.notificationService = notificationService;
    }


    public Object createPushNotification(PushNotificationBean notificationBean) {
        PushNotification pushNotification = convertForCreate(notificationBean);
        pushNotification.setTemplate(templateRepository.getById(notificationBean.getTemplateId()));
        pushNotification.setCustomerType(customerTypeRepository.getById(notificationBean.getTypeId()));
        notificationService.createPushNotification(notificationBean);
        return create(pushNotification);
    }

    public Page<PushNotificationListBean> getPushNotificationList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "dateCreated"));
        Page<PushNotification> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent().stream().map(this::getListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private PushNotificationListBean getListBean(PushNotification pushNotification) {
        PushNotificationListBean listBean = getModelMapper().map(pushNotification, PushNotificationListBean.class);
        listBean.setTemplateName(pushNotification.getTemplate().getTitle());
        listBean.setCustomerType(pushNotification.getCustomerType().getCustomerName());
        return listBean;
    }
}
