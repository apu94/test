package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.CommonResponseBean;
import com.ba.gas.monkey.dtos.district.DistrictBean;
import com.ba.gas.monkey.dtos.district.DistrictListBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.models.District;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("districtService")
public class DistrictService extends BaseService<District, DistrictBean> {

    public DistrictService(BaseRepository<District> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getDropdownList() {
        return getRepository().findAll().stream()
                .filter(District::getStatus)
                .map(this::getDropdownDTO)
                .collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(District district) {
        DropdownDTO dto = getModelMapper().map(district, DropdownDTO.class);
        dto.setValue(district.getName());
        return dto;
    }

    public Page<DistrictListBean> getDistrictList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated")));
        Page<District> page = getRepository().findAll(pageRequest);
        return new PageImpl<>(page.getContent()
                .stream()
                .map(this::getDistrictListBean)
                .sorted(Comparator.comparing(DistrictListBean::getStatus))
                .collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private DistrictListBean getDistrictListBean(District district) {
        DistrictListBean bean = getModelMapper().map(district, DistrictListBean.class);
        bean.setStatus(district.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public Object createDistrict(DistrictBean bean) {
        District district = convertForCreate(bean);
        return create(district);
    }

    public Object deleteDistrict(String id) {
        District district = getRepository().getById(id);
        district.setStatus(Boolean.FALSE);
        update(id, district);
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public Object updateDistrict(String id, DistrictBean bean) {
        District district = getRepository().getById(id);
        BeanUtils.copyProperties(bean,district,AppConstant.IGNORE_PROPERTIES);
        return update(id,district);
    }
}
