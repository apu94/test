package com.ba.gas.monkey.services.customer;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.customer.CustomerTypeBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.CustomerType;
import com.ba.gas.monkey.repositories.CustomerTypeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("customerTypeService")
public class CustomerTypeService extends BaseService<CustomerType, CustomerTypeBean> {

    public CustomerTypeService(BaseRepository<CustomerType> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public List<DropdownDTO> getCustomerTypeDropdownList() {
        List<CustomerType> customerList = getRepository().findAll().stream().filter(c -> c.getCustomerType().contains(AppConstant.CUSTOMER_TYPE)).collect(Collectors.toUnmodifiableList());
        return customerList.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    public DropdownDTO getDropdownDTO(CustomerType type) {
        DropdownDTO dto = getModelMapper().map(type, DropdownDTO.class);
        dto.setValue(type.getCustomerName());
        return dto;
    }

    public CustomerType getByCustomerType(String type) {
        return ((CustomerTypeRepository) getRepository())
                .findByCustomerType(type)
                .orElseThrow(() -> new ServiceExceptionHolder.CustomException(AppConstant.NOT_FOUND_EXCEPTION_CODE, "No type found by : " + type));
    }

    public List<DropdownDTO> getCustomerTypeDropdownAllList() {
        List<CustomerType> customerList = getRepository().findAll();
        return customerList.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }
}
