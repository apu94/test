package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.appmenu.AssignMenuBean;
import com.ba.gas.monkey.dtos.RoleMenuBean;
import com.ba.gas.monkey.dtos.appmenu.AssignMenuCreateBean;
import com.ba.gas.monkey.dtos.appmenu.AssignMenuResponseBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.AppMenu;
import com.ba.gas.monkey.models.GroupInfo;
import com.ba.gas.monkey.models.RoleMenu;
import com.ba.gas.monkey.repositories.RoleMenuRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("roleMenuService")
public class RoleMenuService extends BaseService<RoleMenu, RoleMenuBean> {

    private final GroupInfoService groupInfoService;
    private final AppMenuService appMenuService;

    public RoleMenuService(BaseRepository<RoleMenu> repository, ModelMapper modelMapper,
                           GroupInfoService groupInfoService,
                           AppMenuService appMenuService) {
        super(repository, modelMapper);
        this.groupInfoService = groupInfoService;
        this.appMenuService = appMenuService;
    }

    public AssignMenuResponseBean getAssignMenuList(String roleId) {
        GroupInfo groupInfo = groupInfoService.getRepository().getById(roleId);
        List<RoleMenu> menuList = ((RoleMenuRepository) getRepository()).findByGroupInfo(groupInfo);
        return AssignMenuResponseBean.builder().menuBeans(menuList.stream().map(this::getAssignMenuBean).collect(Collectors.toUnmodifiableList())).build();
    }

    private AssignMenuBean getAssignMenuBean(RoleMenu roleMenu) {
        return AssignMenuBean.builder().tagName(roleMenu.getAppMenu().getTagName()).menuId(roleMenu.getAppMenu().getId()).status(Boolean.TRUE).build();
    }

    public Object saveAssignMenu(String roleId, List<AssignMenuCreateBean> beans) {
        List<String> menuIds = beans.stream().map(AssignMenuCreateBean::getMenuId).collect(Collectors.toUnmodifiableList());
        List<AppMenu> menuList = appMenuService.getRepository().findAllById(menuIds);
        if (menuList.isEmpty()) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.NOT_FOUND_EXCEPTION_CODE, "No menu found");
        }
        GroupInfo groupInfo = groupInfoService.getRepository().getById(roleId);
        List<RoleMenu> assignedMenus = ((RoleMenuRepository) getRepository()).findByGroupInfo(groupInfo);
        getRepository().deleteAll(assignedMenus);
        List<RoleMenuBean> roleMenus = new ArrayList<>();
        menuList.forEach(l -> {
            roleMenus.add(getRoleMenu(l, groupInfo));
        });
        createAll(roleMenus);
        return getAssignMenuList(roleId);
    }

    private RoleMenuBean getRoleMenu(AppMenu appMenu, GroupInfo groupInfo) {
        RoleMenuBean roleMenu = new RoleMenuBean();
        roleMenu.setAppMenu(appMenu);
        roleMenu.setGroupInfo(groupInfo);
        roleMenu.setStatus(Boolean.TRUE);
        return roleMenu;
    }
}
