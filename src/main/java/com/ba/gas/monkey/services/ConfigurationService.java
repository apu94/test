package com.ba.gas.monkey.services;

import com.ba.gas.monkey.dtos.ConfigurationDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@AllArgsConstructor
@Service("configurationService")
public class ConfigurationService {


    private final ServiceChargeService chargeService;
    private final DeliveryScheduleService scheduleService;

    public ConfigurationDTO getConfigurationData() {
            return ConfigurationDTO.builder()
                    .charges(chargeService.getData())
                    .scheduleBean(scheduleService.getData())
                    .build();
    }

    public ConfigurationDTO getUpdateConfigurationData(ConfigurationDTO dto) {
        chargeService.updateData(dto.getCharges());
        scheduleService.updateData(dto.getScheduleBean());
        return ConfigurationDTO.builder()
                .charges(chargeService.getData())
                .scheduleBean(scheduleService.getData())
                .build();
    }
}
