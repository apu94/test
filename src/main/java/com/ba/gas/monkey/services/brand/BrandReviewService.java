package com.ba.gas.monkey.services.brand;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.BrandReviewInfoBean;
import com.ba.gas.monkey.dtos.brand.BrandReviewBean;
import com.ba.gas.monkey.models.Brand;
import com.ba.gas.monkey.models.BrandReview;
import com.ba.gas.monkey.repositories.BrandReviewRepository;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.utility.AuthenticationUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("brandReviewService")
public class BrandReviewService extends BaseService<BrandReview, BrandReviewBean> {

    private final CustomerService customerService;
    private final BrandService brandService;

    public BrandReviewService(BaseRepository<BrandReview> repository, ModelMapper modelMapper,
                              CustomerService customerService,
                              BrandService brandService) {
        super(repository, modelMapper);
        this.customerService = customerService;
        this.brandService = brandService;
    }

    public Object saveReview(BrandReviewBean bean) {
        String customerId = Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId();
        BrandReview brandReview = convertForCreate(bean);
        brandReview.setCustomer(customerService.getRepository().getById(customerId));
        brandReview.setBrand(brandService.getRepository().getById(bean.getBrandId()));
        brandReview.setReviewDate(LocalDateTime.now());
        brandReview.setDateCreated(Instant.now());
        brandReview.setDateModified(Instant.now());
        brandReview.setUpdtId(customerId);
        brandReview.setStatus(Boolean.TRUE);
        brandReview.setReviewRead(Boolean.FALSE);
        return convertForRead(getRepository().save(brandReview));
    }

    public List<BrandReviewInfoBean> brandReviewList(String id) {
        Brand brand = brandService.getRepository().getById(id);
        List<BrandReview> list = ((BrandReviewRepository) getRepository()).findByBrand(brand);
        return list.stream().map(this::getBrandReviewInfoBean).collect(Collectors.toUnmodifiableList());
    }

    private BrandReviewInfoBean getBrandReviewInfoBean(BrandReview brandReview) {
        return BrandReviewInfoBean.builder()
                .customerName(brandReview.getCustomer().getCustomerName())
                .photoLink(brandReview.getCustomer().getPhotoLink())
                .ratingPoint(brandReview.getReviewRating())
                .description(brandReview.getDescription())
                .build();
    }
}
