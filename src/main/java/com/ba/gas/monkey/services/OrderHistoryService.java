package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.dtos.order.OrderHistoryBean;
import com.ba.gas.monkey.models.Customer;
import com.ba.gas.monkey.models.OrderHistory;
import com.ba.gas.monkey.models.OrderInfo;
import com.ba.gas.monkey.projection.OrderHistoryProjectionBean;
import com.ba.gas.monkey.repositories.OrderHistoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service("orderHistoryService")
public class OrderHistoryService extends BaseService<OrderHistory, OrderHistoryBean> {
    public OrderHistoryService(BaseRepository<OrderHistory> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public void saveHistory(OrderHistory orderHistory) {
        List<OrderHistory> list = ((OrderHistoryRepository) getRepository()).findByOrderInfo(orderHistory.getOrderInfo());
        orderHistory.setSlNo(list.size() + 1);
        getRepository().save(orderHistory);
    }

    public List<OrderInfo> getOrderList(Customer partner) {
        return ((OrderHistoryRepository) getRepository()).findByPartner(partner)
                .stream().filter(distinctByKey(OrderHistory::getOrderInfo))
                .map(OrderHistory::getOrderInfo).collect(Collectors.toUnmodifiableList());
    }

    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public List<OrderHistoryProjectionBean> getOrderHistory(String id) {
        return ((OrderHistoryRepository) getRepository()).getOrderHistoryById(id);
    }
}
