package com.ba.gas.monkey.services.order;

import com.ba.gas.monkey.base.BaseEntity;
import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.*;
import com.ba.gas.monkey.dtos.customer.CustomerBean;
import com.ba.gas.monkey.dtos.order.*;
import com.ba.gas.monkey.dtos.partner.PartnerDetailsBean;
import com.ba.gas.monkey.dtos.transaction.TransactionCreateBean;
import com.ba.gas.monkey.exception.OrderExceptionHolder;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.*;
import com.ba.gas.monkey.repositories.*;
import com.ba.gas.monkey.services.*;
import com.ba.gas.monkey.services.cart.CartDetailService;
import com.ba.gas.monkey.services.cart.CartService;
import com.ba.gas.monkey.services.customer.CustomerService;
import com.ba.gas.monkey.services.firebase.FirebaseMessagingService;
import com.ba.gas.monkey.services.notification.AppNotificationService;
import com.ba.gas.monkey.services.partner.PartnerDetailsService;
import com.ba.gas.monkey.services.user.UserInfoService;
import com.ba.gas.monkey.utility.*;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service("orderService")
public class OrderService extends BaseService<OrderInfo, OrderBean> {

    private final CartService cartService;
    private final CartDetailService cartDetailService;
    private final OrderRepository orderRepository;
    private final OrderProductService orderProductService;
    private final DistrictService districtService;
    private final ThanaService thanaService;
    private final ClusterService clusterService;
    private final CustomerService customerService;
    private final UserInfoService userInfoService;
    private final CouponService couponService;
    private final DeviceTokenService deviceTokenService;
    private final TransactionService transactionService;
    private final AppNotificationService appNotificationService;
    private final FirebaseMessagingService firebaseMessagingService;
    private final PartnerDetailsService partnerDetailsService;
    private final ServiceChargeService serviceChargeService;
    private final DeliveryScheduleUtils deliveryScheduleUtils;
    private final RedisTemplate<String, Object> template;

    private final DealerDetailsRepository dealerDetailsRepository;
    private final OrderHistoryService orderHistoryService;

    public OrderService(BaseRepository<OrderInfo> repository, ModelMapper modelMapper, CartService cartService, CartDetailService cartDetailService, OrderRepository orderRepository, OrderProductService orderProductService, DistrictService districtService, ThanaService thanaService, ClusterService clusterService, CustomerService customerService, UserInfoService userInfoService, CouponService couponService, ServiceChargeService serviceChargeService, RedisTemplate<String, Object> template, DeviceTokenService deviceTokenService, FirebaseMessagingService firebaseMessagingService, PartnerDetailsService partnerDetailsService, AppNotificationService appNotificationService, TransactionService transactionService, DealerDetailsRepository dealerDetailsRepository, DeliveryScheduleUtils deliveryScheduleUtils, OrderHistoryService orderHistoryService) {
        super(repository, modelMapper);
        this.cartService = cartService;
        this.cartDetailService = cartDetailService;
        this.orderRepository = orderRepository;
        this.orderProductService = orderProductService;
        this.districtService = districtService;
        this.thanaService = thanaService;
        this.clusterService = clusterService;
        this.customerService = customerService;
        this.userInfoService = userInfoService;
        this.couponService = couponService;
        this.serviceChargeService = serviceChargeService;
        this.template = template;
        this.deviceTokenService = deviceTokenService;
        this.firebaseMessagingService = firebaseMessagingService;
        this.partnerDetailsService = partnerDetailsService;
        this.appNotificationService = appNotificationService;
        this.transactionService = transactionService;
        this.deliveryScheduleUtils = deliveryScheduleUtils;
        this.dealerDetailsRepository = dealerDetailsRepository;
        this.orderHistoryService = orderHistoryService;
    }

    public OrderBean createOrder(CreateOrderRequestBean bean) {
        log.info("create-order-bean================>>>>>>>>>>>{}", bean);
        Cart cart = cartService.getRepository().getById(bean.getCartId());
        if (cart.getCartDetailList().isEmpty()) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.NOT_FOUND_EXCEPTION_CODE, "Product not found in cart.");
        }
        OrderInfo orderInfo = getModelMapper().map(bean, OrderInfo.class);
        orderInfo.setDistrict(districtService.getRepository().getById(bean.getDistrictId()));
        orderInfo.setThana(thanaService.getRepository().getById(bean.getThanaId()));
        orderInfo.setCluster(clusterService.getRepository().getById(bean.getClusterId()));
        orderInfo.setCustomer(customerService.getRepository().getById(bean.getCustomerId()));
        orderInfo.setDateCreated(Instant.now());
        orderInfo.setDateModified(Instant.now());
        orderInfo.setOrderNumber(AppUtilities.getNext());
        String updtId = userInfoService.getByEmailAddress("sadmin@batworld.com").get().getId();
        orderInfo.setUpdtId(updtId);
        orderInfo.setStatus(Boolean.TRUE);
        Double commissionPercentage = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("PARTNER_COMMISSION").get().getServiceValue();
        Double partnerCommission = (commissionPercentage * orderInfo.getServiceCharge()) / 100;
        orderInfo.setPartnerCharge(partnerCommission);
        orderInfo.setOwnerCharge(orderInfo.getServiceCharge() - partnerCommission);
        orderInfo.setPaymentStatus(false);
        OrderInfo createdOrderInfo = getRepository().save(orderInfo);
        log.info("createdOrderInfo-bean================>>>>>>>>>>>{}", createdOrderInfo);
        orderHistoryService.saveHistory(getOrderHistoryBean(createdOrderInfo, Constant.CREATED));
        cart.getCartDetailList().forEach(cartDetail -> {
            OrderProduct orderProduct = getOrderProduct(cartDetail, createdOrderInfo);
            orderProduct.setUpdtId(updtId);
            orderProductService.getRepository().save(orderProduct);
        });
        cartDetailService.getRepository().deleteAll(cart.getCartDetailList());
        Customer customer = customerService.getRepository().getById(bean.getCustomerId());
        customer.setRewardPoint(customer.getRewardPoint());
        customerService.getRepository().save(customer);
        if (!bean.getCouponCode().equalsIgnoreCase("") && bean.getCouponCode() != null) {
            Coupon coupon = ((CouponRepository) couponService.getRepository()).findByCouponCode(bean.getCouponCode()).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No coupon found by code:" + bean.getCouponCode()));
            coupon.setCouponUsed(1);
            couponService.getRepository().save(coupon);
        }
        OrderInfo orderInfoResponse = orderRepository.getById(createdOrderInfo.getId());
        if (orderInfoResponse.getRegularDelivery()) {
            if (deliveryScheduleUtils.scheduleCheck(LocalDateTime.now())) {
                template.opsForValue().set(orderInfoResponse.getId() + "-1", orderInfoResponse.getId());
                template.expire(orderInfoResponse.getId() + "-1", Constant.TTL, TimeUnit.MINUTES);
                sendOderNotificationToPartnerAndCustomer(createdOrderInfo);
            } else {
                template.opsForValue().set(orderInfoResponse.getId() + "-1", orderInfoResponse.getId());
                template.expire(orderInfoResponse.getId() + "-1", deliveryScheduleUtils.getNextOpeningTime(LocalDateTime.now()), TimeUnit.MINUTES);
            }
        } else {
            template.opsForValue().set(orderInfoResponse.getId() + "-1", orderInfoResponse.getId());
            template.expire(orderInfoResponse.getId() + "-1", deliveryScheduleUtils.getExpressDeliveryInMinute(createdOrderInfo.getDeliveryDate(), createdOrderInfo.getDeliverySlot()), TimeUnit.MINUTES);
        }
        log.info("before customer order notification================>>>>>>>>>>>{}", createdOrderInfo);
        appNotificationService.customerOrderConfirmation(createdOrderInfo);
//        orderHistoryRepository.save(getOrderHistoryBean(createdOrderInfo,Constant.RECEIVED));
        return getModelMapper().map(orderInfoResponse, OrderBean.class);
    }

    private OrderHistory getOrderHistoryBean(OrderInfo orderInfo, String orderStatus) {
        OrderHistory historyBean = new OrderHistory();
        historyBean.setOrderStatus(orderStatus);
        historyBean.setPartner(orderInfo.getPartner());
        historyBean.setOrderInfo(orderInfo);
        historyBean.setStatus(Boolean.TRUE);
        historyBean.setDateCreated(Instant.now());
        historyBean.setDateModified(Instant.now());
        historyBean.setUpdtId(orderInfo.getUpdtId());
        return historyBean;
    }

    public List<OrderBean> getOrderList(String id) {
        List<OrderInfo> list = orderRepository.findByCustomerId(id, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
        });
        return listBean;
    }

    public Page<OrderInfoListBean> getOrderList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated")));
        Page<OrderInfo> page = getRepository().findAll(pageRequest);
        List<OrderInfoListBean> listBeans = page.getContent().stream().sorted(Comparator.comparing(BaseEntity::getStatus).reversed()).map(this::getOrderInfoListBean).collect(Collectors.toUnmodifiableList());
        return new PageImpl<>(listBeans, page.getPageable(), page.getTotalElements());
    }

    private OrderInfoListBean getOrderInfoListBean(OrderInfo orderInfo) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
        Date myDate = Date.from(orderInfo.getDateCreated());
        SimpleDateFormat formatterTime = new SimpleDateFormat("hh:mm:ss");
        System.out.println("============>>>>>>" + orderInfo.getId());
        String productImage = (orderInfo.getOrderProductList().get(0).getBuyProduct().getProductImageList().isEmpty()) ? "" : orderInfo.getOrderProductList().get(0).getBuyProduct().getProductImageList().get(0).getImageLink();
        List<String> orderTypes = new ArrayList<>();
        for (OrderProduct orderProduct : orderInfo.getOrderProductList()) {
            Optional<Product> optional = Optional.ofNullable(orderProduct.getReturnProduct());
            orderTypes.add(optional.isEmpty() ? AppConstant.ORDER_TYPE_PACKAGE : AppConstant.ORDER_TYPE_REFILL);
        }
        return OrderInfoListBean.builder().id(orderInfo.getId()).orderNumber(orderInfo.getOrderNumber()).customerName(orderInfo.getCustomer().getCustomerName()).partnerName("N/A").dealerName("N/A").customerPhoneNo(orderInfo.getCustomer().getPhoneNo()).productPhoto(productImage).brandName(orderInfo.getOrderProductList().get(0).getBuyProduct().getBrand().getNameEn()).orderQuantity(orderInfo.getOrderProductList().get(0).getQuantity()).productSize(orderInfo.getOrderProductList().get(0).getBuyProduct().getProductSize().getNameEn()).price(orderInfo.getTotal()).valveSize(orderInfo.getOrderProductList().get(0).getBuyProduct().getProductValveSize().getNameEn()).orderDate(formatter.format(myDate)).orderStatus(orderInfo.getOrderStatus()).orderCreatedAt(orderInfo.getDateCreated()).deliveredAt(orderInfo.getDeliveredAt()).pickedAt(orderInfo.getPickedAt()).cylinderReturnedAt(orderInfo.getCylinderReturnedAt()).orderType(String.join(",", orderTypes)).build();
    }

    public OrderBean getOrder(String id) {
        OrderInfo orderInfo = orderRepository.getById(id);
        OrderBean bean = getModelMapper().map(orderInfo, OrderBean.class);
        Double vatPercentage = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("VAT_RATE").get().getServiceValue();
        bean.setVatPercent(vatPercentage);
        Double exchangeRate = ((ServiceChargeRepository) serviceChargeService.getRepository()).findByServiceName("EXCHANGE_RATE").get().getServiceValue();
        bean.setExchange(exchangeRate);
        bean.setDeliveryDate(orderInfo.getDeliveryDate());
        return bean;
    }

    public AdminOrderDetailsBean getAdminOrder(String id) {
        Optional<OrderInfo> optional = orderRepository.findById(id);
        if (optional.isEmpty()) {
            return null;
        }
        return getAdminOrderDetailBean(optional.get());
    }

    private AdminOrderDetailsBean getAdminOrderDetailBean(OrderInfo orderInfo) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat formatterTime = new SimpleDateFormat("hh:mm:ss");
        Date myDate = Date.from(orderInfo.getDateCreated());
        Optional<Customer> partner = Optional.ofNullable(orderInfo.getPartner());
        Optional<Customer> dealer = Optional.ofNullable(orderInfo.getDealer());
        String dealerInfo = dealer.isPresent() ? dealerDetailsRepository.findByCustomerId(dealer.get().getId()).getShopName() : "";
        List<OrderProduct> orderProducts = ((OrderProductRepository) orderProductService.getRepository()).findByOrderInfo(orderInfo);
        List<OrderedProductBean> productBeans = orderProducts.stream().map(this::getOrderedProductBean).collect(Collectors.toUnmodifiableList());
        Double exchangeRate = orderInfo.getSubTotal() - productBeans.stream().mapToDouble(OrderedProductBean::getProductPrice).sum();
        Optional<String> opDeliveryArea = Optional.ofNullable(orderInfo.getDeliveryArea());
        List<OrderHistory> orderHistories = ((OrderHistoryRepository) orderHistoryService.getRepository()).findByOrderInfo(orderInfo);
        List<OrderProduct> orderProductList = orderInfo.getOrderProductList();
        List<String> orderTypes = new ArrayList<>();
        for (OrderProduct orderProduct : orderProductList) {
            Optional<Product> optional = Optional.ofNullable(orderProduct.getReturnProduct());
            orderTypes.add(optional.isEmpty() ? AppConstant.ORDER_TYPE_PACKAGE : AppConstant.ORDER_TYPE_REFILL);
        }
        return AdminOrderDetailsBean.builder()
                .id(orderInfo.getId()).customerId(orderInfo.getCustomer().getId())
                .orderNumber(orderInfo.getOrderNumber())
                .customerName(orderInfo.getCustomer().getCustomerName())
                .partnerName(partner.isPresent() ? partner.get().getCustomerName() : "")
                .dealerName(dealer.isPresent() ? dealer.get().getCustomerName() : "")
                .phoneNo(orderInfo.getCustomer().getPhoneNo())
                .address((opDeliveryArea.map(s -> s + ",").orElse("")) + orderInfo.getCluster().getName() + ", " + orderInfo.getThana().getName() + ", " + orderInfo.getDistrict().getName())
                .productDescription(orderProductList.get(0).getBuyProduct().getBrand().getCompanyName() + ", " + orderProductList.get(0).getBuyProduct().getBrand().getNameEn() + ", " + orderProductList.get(0).getBuyProduct().getBrand().getNameBn())
                .orderQty(orderProductList.get(0).getQuantity())
                .deliveryMapAddress(orderInfo.getDeliveryMapAddress())
                .dateCreated(formatter.format(myDate))
                .orderTime(formatterTime.format(myDate))
                .note(orderInfo.getNote())
                .returnProduct(orderProductList.get(0).getReturnProduct() != null ? orderProductList.get(0).getBuyProduct().getBrand().getCompanyName() + ", " + orderProductList.get(0).getBuyProduct().getBrand().getNameEn() + ", " + orderProductList.get(0).getBuyProduct().getBrand().getNameBn() : "")
                .orderStatus(orderInfo.getOrderStatus())
                .subTotal(orderInfo.getSubTotal())
                .vat(orderInfo.getVat())
                .serviceCharge(orderInfo.getServiceCharge())
                .discountAmount(orderInfo.getDiscountAmount())
                .total(orderInfo.getTotal())
                .couponValue(orderInfo.getCouponValue())
                .regularDelivery(orderInfo.getRegularDelivery())
                .products(productBeans)
                .pickupAddress(dealerInfo)
                .exchangeRate(exchangeRate)
                .paymentType(orderInfo.getDeliveryPaymentType())
                .paymentStatus(orderInfo.getPaymentStatus() ? AppConstant.PAID : AppConstant.PENDING)
                .orderHistoryBeans(getOrderHistoryListBean(orderHistories))
                .partnerMobileNo(partner.isPresent() ? partner.get().getPhoneNo() : "")
                .dealerMobileNo(dealer.isPresent() ? dealer.get().getPhoneNo() : "")
                .dealerAddress(dealer.isPresent() ? dealer.get().getAddress() : "")
                .totalConvenienceFee(orderInfo.getTotalConvenienceFee())
                .totalExchange(orderInfo.getTotalExchange())
                .orderCreatedDate(orderInfo.getDateCreated())
                .orderType(String.join(",", orderTypes))
                .build();
    }


    private List<OrderHistoryInfoBean> getOrderHistoryListBean(List<OrderHistory> orderHistories) {
        List<OrderHistoryInfoBean> beans = new ArrayList<>();
        int i = 1;
        for (OrderHistory history : orderHistories) {
            String orderStatus = history.getOrderStatus();
            Optional<Customer> partner = Optional.ofNullable(history.getPartner());
            String response = partner.isPresent() ? partner.get().getCustomerName() : AppConstant.EMPTY_STRING;
            String orderStatusText = AppConstant.ORDER_STATUS_MAP.getOrDefault(orderStatus, AppConstant.EMPTY_STRING);
            if (orderStatus.equalsIgnoreCase(Constant.PENDING) || orderStatus.equalsIgnoreCase(Constant.ACCEPT)) {
                orderStatusText = AppConstant.ORDER_STATUS_TAG_MAP.getOrDefault(i, "Admin ") + " " + orderStatusText;
                i++;
            }
            if (orderStatus.equalsIgnoreCase(Constant.CREATED)) {
                response = "GMBL";
            }
            OrderHistoryInfoBean infoBean = OrderHistoryInfoBean.builder()
                    .orderStatus(orderStatusText)
                    .orderAction(AppConstant.ORDER_ACTION_MAP.getOrDefault(orderStatus, AppConstant.EMPTY_STRING))
                    .actionTime(history.getDateModified())
                    .response(response)
                    .build();
            beans.add(infoBean);
        }
        return beans;
    }

    private OrderedProductBean getOrderedProductBean(OrderProduct orderProduct) {
        String productName = orderProduct.getBuyProduct().getBrand().getNameEn() + " " + orderProduct.getBuyProduct().getProductSize().getNameEn();
        Optional<Product> returnProduct = Optional.ofNullable(orderProduct.getReturnProduct());
        Integer quantity = orderProduct.getQuantity();
        Double price = orderProduct.getBuyProduct().getProductPrice().getPackagePrice();
        if (returnProduct.isPresent()) {
            price = orderProduct.getBuyProduct().getProductPrice().getRefillPrice();
        }
        return OrderedProductBean.builder().productName(productName).productPrice(price).quantity(quantity).build();
    }

    private OrderProduct getOrderProduct(CartDetail cartDetail, OrderInfo orderInfo) {
        double purchasePrice = cartDetail.getReturnProduct() != null ? cartDetail.getBuyProduct().getProductPrice().getRefillPrice() : cartDetail.getBuyProduct().getProductPrice().getPackagePrice();
        double exchangeAmount;
        if (cartDetail.getReturnProduct() != null && (cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() > cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice())) {
            exchangeAmount = cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice() - cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice();
        } else {
            exchangeAmount = 0;
        }

        return OrderProduct.builder().orderInfo(orderInfo).buyProduct(cartDetail.getBuyProduct()).returnProduct(cartDetail.getReturnProduct()).quantity(cartDetail.getQuantity()).refillPrice(cartDetail.getBuyProduct().getProductPrice().getRefillPrice()).emptyCylinderPrice(cartDetail.getBuyProduct().getProductPrice().getEmptyCylinderPrice()).productPurchasePrice(purchasePrice).returnEmptyCylinderPrice(cartDetail.getReturnProduct() != null ? cartDetail.getReturnProduct().getProductPrice().getEmptyCylinderPrice() : 0).exchangeAmount(exchangeAmount).build();
    }

    public List<PartnerDetailsBean> getNearByPartner(OrderInfo orderInfo, int number) {
        Optional<ServiceCharge> optional = serviceChargeService.getByServiceType(AppConstant.SEARCH_AREA_IN_KM);
        Double distance = (optional.isPresent()) ? optional.get().getServiceValue() : AppConstant.DEFAULT_DISTANCE_FOR_SEARCH;
        DistanceBean distanceBean = new DistanceBean();
        distanceBean.setDistanceInKM(distance);
        distanceBean.setLatitude(orderInfo.getDeliveryLat());
        distanceBean.setLongitude(orderInfo.getDeliveryLong());
        PageRequest pageRequest = PageRequest.of(0, 1);
        List<String> partnerIds = ((OrderHistoryRepository) orderHistoryService.getRepository()).findByOrderInfo(orderInfo).stream().map(OrderHistory::getPartner).filter(Objects::nonNull).map(Customer::getId).collect(Collectors.toUnmodifiableList());
        return partnerDetailsService.getNearestPartner(distanceBean, partnerIds, pageRequest).getContent();
    }


    public OrderBean updateOrderStatus(OrderStatusUpdateBean bean) {
        OrderInfo orderInfo = getRepository().getById(bean.getOrderId());
        String orderStatus = bean.getOrderStatus();
        if (orderStatus.equalsIgnoreCase(Constant.ACCEPT)) {
            if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)) {
                throw new OrderExceptionHolder.CustomException(OrderExceptionHolder.EXCEPTION_ORDER_CANCELED, "Customer canceled order.");
            }
            Customer customer = customerService.getRepository().getById(bean.getPartnerId());
            orderInfo.setPartner(customer);
            orderInfo.setAcceptedAt(Instant.now());
            customer.setUserStatus(AppConstant.USER_STATUS_PROCESSING);
            customerService.getRepository().save(customer);
            orderHistoryService.saveHistory(getOrderHistoryBean(orderInfo, Constant.ACCEPT));
        } else if (orderStatus.equalsIgnoreCase(Constant.PICKED)) {
            if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)) {
                throw new OrderExceptionHolder.CustomException(OrderExceptionHolder.EXCEPTION_ORDER_CANCELED, "Customer canceled order.");
            }
            orderInfo.setDealer(customerService.getRepository().getById(bean.getDealerId()));
            orderInfo.setPickedAt(Instant.now());
            appNotificationService.customerOrderPicked(orderInfo);
            appNotificationService.dealerOrderPicked(orderInfo);
            orderHistoryService.saveHistory(getOrderHistoryBean(orderInfo, Constant.PICKED));

        } else if (orderStatus.equalsIgnoreCase(Constant.DELIVERED)) {
            orderInfo.setDeliveredAt(Instant.now());
            PartnerDetailsBean details = partnerDetailsService.getPartnerDetails(bean.getPartnerId());
            details.setTotalEarnings(details.getTotalEarnings() + orderInfo.getPartnerCharge());
            details.setTotalPayable(details.getTotalPayable() + orderInfo.getOwnerCharge());
            partnerDetailsService.updatePartnerDetails(details);
            if (orderInfo.getDeliveryPaymentType().equalsIgnoreCase("Cash")) {
                orderInfo.setPaymentStatus(Boolean.TRUE);
            }
            TransactionCreateBean transactionCreateBean = new TransactionCreateBean();
            transactionCreateBean.setAmount(orderInfo.getOwnerCharge());
            transactionCreateBean.setNote("Order Number: " + orderInfo.getOrderNumber());
            transactionCreateBean.setCustomerId(orderInfo.getPartner().getId());
            transactionService.addAmount(transactionCreateBean);
            Customer partner = customerService.getRepository().getById(bean.getPartnerId());
            int rewardPoint = Optional.ofNullable(partner.getRewardPoint()).orElse(0);
            partner.setRewardPoint(rewardPoint + 1);
            customerService.getRepository().save(partner);
            appNotificationService.customerOrderDelivered(orderInfo);
            orderHistoryService.saveHistory(getOrderHistoryBean(orderInfo, Constant.DELIVERED));
        } else if (orderStatus.equalsIgnoreCase(Constant.RECEIVED)) {
            orderInfo.setReceivedAt(Instant.now());
            appNotificationService.partnerOrderReceived(orderInfo);
            OrderHistory orderHistoryBean = getOrderHistoryBean(orderInfo, Constant.RECEIVED);
            orderHistoryBean.setPartner(orderInfo.getCustomer());
            orderHistoryService.saveHistory(orderHistoryBean);
        } else if (orderStatus.equalsIgnoreCase(Constant.COMPLETED)) {
            appNotificationService.dealerOrderEmptyCylinder(orderInfo);
            Customer customer = customerService.getRepository().getById(bean.getPartnerId());
            customerService.updateRewardPoint(orderInfo.getCustomer().getId());//customer
            customer.setUserStatus(AppConstant.ACTIVE.toLowerCase());
            customerService.getRepository().save(customer);
            orderHistoryService.saveHistory(getOrderHistoryBean(orderInfo, Constant.COMPLETED));
        } else if (orderStatus.equalsIgnoreCase(Constant.CANCELED)) {
            if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.PICKED) || orderInfo.getOrderStatus().equalsIgnoreCase(Constant.RECEIVED) || orderInfo.getOrderStatus().equalsIgnoreCase(Constant.DELIVERED)) {
                throw new OrderExceptionHolder.CustomException(OrderExceptionHolder.EXCEPTION_ORDER_CANCELED, "Cancel not possible right now. Order already picked by partner.");
            }
            orderInfo.setCancelReason(bean.getCancelReason());
            appNotificationService.customerOrderCanceled(orderInfo);
            if (orderInfo.getPartner() != null) {
                appNotificationService.partnerOrderCanceled(orderInfo);
            }
        } else if (orderStatus.equalsIgnoreCase(Constant.REJECT)) {
            System.out.println("inside reject: ");
            orderHistoryService.saveHistory(getOrderHistoryBean(orderInfo, Constant.REJECT));
//            Optional<OrderHistory> optionalOrderHistory = ((OrderHistoryRepository) orderHistoryService.getRepository()).findByOrderInfoAndPartner(orderInfo, orderInfo.getPartner());
//            if (optionalOrderHistory.isPresent()) {
//                OrderHistory orderHistory = optionalOrderHistory.get();
//                orderHistory.setOrderStatus(orderStatus);
//                orderHistory.setDateModified(Instant.now());
//                orderHistoryService.saveHistory(orderHistory);
//            }
            Set<String> redisKeys = template.keys(orderInfo.getId() + "*");
            List<String> keysList = new ArrayList<>();
            assert redisKeys != null;
            for (String data : redisKeys) {
                System.out.println("keyyyyyy: " + data);
                keysList.add(data);
            }
            if (!keysList.isEmpty()) {
                OrderInfo updatedOrderInfo = null;
                String fullKey = keysList.get(0);
                String key = fullKey.substring(0, fullKey.length() - 2);
                int count = Integer.parseInt(fullKey.substring(fullKey.lastIndexOf('-') + 1));
                if (count < 3) {
                    List<PartnerDetailsBean> nearByPartnerList = getNearByPartner(orderInfo, count + 1);
                    log.info("radis-Partner-found=============>>>{}", nearByPartnerList.size());
                    if (nearByPartnerList.size() > 0) {
                        log.info("radis-Partner-found=============>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getId());
                        log.info("radis-Partner-found=============>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getCustomerName());
                        orderInfo.setPartner(customerService.getRepository().getById(nearByPartnerList.get(0).getCustomerInfoBean().getId()));
                        orderInfo.setOrderStatus(Constant.PENDING);
                        orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
                        updatedOrderInfo = orderRepository.save(orderInfo);
                        appNotificationService.sendOrderActionNotification(orderInfo, nearByPartnerList.get(0));
                        orderHistoryService.saveHistory(getOrderHistoryBean(updatedOrderInfo, Constant.PENDING));
                    } else {
                        orderInfo.setOrderStatus(Constant.NO_USER_FOUND);
                        orderInfo.setPartner(null);
                        orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
                        updatedOrderInfo = orderRepository.save(orderInfo);
                        orderHistoryService.saveHistory(getOrderHistoryBean(updatedOrderInfo, Constant.NO_USER_FOUND));
                    }
                    count++;
                    template.delete(fullKey);
                    String newKey = key + "-" + count;
                    template.opsForValue().set(newKey, key);
                    template.expire(newKey, Constant.TTL, TimeUnit.MINUTES);
                }
            } else {
                orderInfo.setPartner(null);
            }
        }
        orderInfo.setOrderStatus(orderStatus);
        OrderInfo updatedOrderInfo = getRepository().save(orderInfo);
//        if (orderStatus.equalsIgnoreCase(Constant.ACCEPT)) {
//            List<OrderHistory> histories = ((OrderHistoryRepository) orderHistoryService.getRepository()).findByOrderInfo(orderInfo);
//            Optional<OrderHistory> orderHistory = histories.stream().filter(h -> h.getOrderStatus().equalsIgnoreCase(Constant.ACCEPT)).findAny();
//            if(orderHistory.is){
//
//            }
//            orderHistoryService.saveHistory(getOrderHistoryBean(orderInfo, Constant.ACCEPT));
////            Optional<OrderHistory> optionalOrderHistory = ((OrderHistoryRepository) orderHistoryService.getRepository()).findByOrderInfoAndPartner(orderInfo, orderInfo.getPartner());
////            if (optionalOrderHistory.isPresent()) {
////                OrderHistory orderHistory = optionalOrderHistory.get();
////                orderHistory.setOrderStatus(orderInfo.getOrderStatus());
////                orderHistory.setDateModified(Instant.now());
////                orderHistoryService.saveHistory(orderHistory);
////            }
//        }
        return getModelMapper().map(updatedOrderInfo, OrderBean.class);
    }

    public List<OrderBean> getRequestedOrderList(String id) {
        List<OrderInfo> list = orderRepository.findByPartnerId(id, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.REJECT) || orderInfo.getOrderStatus().equalsIgnoreCase(Constant.PENDING) || orderInfo.getOrderStatus().equalsIgnoreCase(Constant.FORWARD)) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public List<OrderBean> getPastOrderList(String id) {
        String partnerId = Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId();
        List<OrderInfo> list = orderRepository.findByPartnerId(partnerId, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(BaseEntity::getStatus).collect(Collectors.toUnmodifiableList());
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED)) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public List<OrderBean> getCustomerPastOrderList(String id) {
        List<OrderInfo> list = orderRepository.findByCustomerId(id, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(o -> !o.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)).filter(OrderInfo::getStatus).collect(Collectors.toUnmodifiableList());
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED)) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public List<OrderBean> getRunningOrderList(String id) {
        String partnerId = Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId();
        List<OrderInfo> list = orderRepository.findByPartnerId(partnerId, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(BaseEntity::getStatus).collect(Collectors.toUnmodifiableList());
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.ACCEPT) || orderStatus.equalsIgnoreCase(Constant.PICKED) || orderStatus.equalsIgnoreCase(Constant.DELIVERED) || orderStatus.equalsIgnoreCase(Constant.RECEIVED)) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public List<OrderBean> getCustomerRunningOrderList(String id) {
        List<OrderInfo> list = orderRepository.findByCustomerId(id, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(o -> !o.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)).filter(OrderInfo::getStatus).collect(Collectors.toUnmodifiableList());
        List<OrderBean> listBean = new ArrayList<>();
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.REJECT) || orderStatus.equalsIgnoreCase(Constant.NO_USER_FOUND) || orderStatus.equalsIgnoreCase(Constant.NO_ACTION) || orderStatus.equalsIgnoreCase(Constant.PENDING) || orderStatus.equalsIgnoreCase(Constant.ACCEPT) || orderStatus.equalsIgnoreCase(Constant.PICKED) || orderStatus.equalsIgnoreCase(Constant.DELIVERED) || orderStatus.equalsIgnoreCase(Constant.FORWARD) || orderStatus.equalsIgnoreCase(Constant.CANCELED) || orderStatus.equalsIgnoreCase(Constant.RECEIVED)) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public OrderBean forwardOrder(ForwardOrderBean bean) {
        OrderInfo orderInfo = getRepository().getById(bean.getOrderId());
        if (orderInfo.getOrderStatus().equalsIgnoreCase(Constant.CANCELED)) {
            throw new OrderExceptionHolder.CustomException(OrderExceptionHolder.EXCEPTION_ORDER_CANCELED, "Customer canceled order.");
        }
        orderInfo.setPartner(customerService.getRepository().getById(bean.getPartnerId()));
        orderInfo.setOrderStatus(Constant.FORWARD);
        getRepository().save(orderInfo);
        appNotificationService.sendOrderActionNotification(orderInfo, partnerDetailsService.getPartnerDetails(bean.getPartnerId()));
        return getModelMapper().map(getRepository().save(orderInfo), OrderBean.class);
    }

    public List<OrderInfoBean> dealerCompletedOrderList() {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        List<OrderInfo> list = ((OrderRepository) getRepository()).findByDealerAndOrderStatus(customer, AppConstant.ORDER_STATUS_COMPLETED.toLowerCase(), Sort.by(Sort.Direction.DESC, "dateCreated"));
        return list.stream().map(l -> getModelMapper().map(l, OrderInfoBean.class)).collect(Collectors.toUnmodifiableList());
    }

    public List<OrderInfoBean> dealerOrderList() {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        List<OrderInfo> list = ((OrderRepository) getRepository()).findByDealer(customer, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(f -> !f.getOrderStatus().equals(AppConstant.ORDER_STATUS_COMPLETED.toLowerCase())).collect(Collectors.toUnmodifiableList());
        return list.stream().map(l -> getModelMapper().map(l, OrderInfoBean.class)).collect(Collectors.toUnmodifiableList());
    }

    public List<OrderBean> getPartnerPastOrderListCurrentMonth(String id) {
        List<OrderInfo> list = orderRepository.findByPartnerId(id, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(BaseEntity::getStatus).collect(Collectors.toUnmodifiableList());
        List<OrderBean> listBean = new ArrayList<>();
        LocalDate firstDayOfRunningMonth = LocalDate.now().withDayOfMonth(1);
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateAfter(firstDayOfRunningMonth, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public List<OrderBean> getDealerPastOrderListCurrentMonth(String id) {
        Customer customer = customerService.getRepository().getById(Objects.requireNonNull(AuthenticationUtils.getCustomerDetails()).getId());
        List<OrderInfo> list = orderRepository.findByDealer(customer, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<OrderBean> listBean = new ArrayList<>();
        LocalDate firstDayOfRunningMonth = LocalDate.now().withDayOfMonth(1);
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateAfter(firstDayOfRunningMonth, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        return listBean;
    }

    public TransactionSummaryBean getPartnerTransactionSummaryMonth(String id, String date) {
        TransactionSummaryBean summaryBean = new TransactionSummaryBean();
        summaryBean.setTotalEarning(0d);
        List<OrderInfo> list = orderRepository.findByPartnerId(id, Sort.by(Sort.Direction.DESC, "dateCreated")).stream().filter(BaseEntity::getStatus).collect(Collectors.toUnmodifiableList());
        List<OrderBean> listBean = new ArrayList<>();
        LocalDate dateTime = LocalDate.parse(date);
        LocalDate minDate = dateTime.withDayOfMonth(1);
        LocalDate maxDate = dateTime.withDayOfMonth(dateTime.getMonth().length(dateTime.isLeapYear()));
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateInBetweenIncludingEndPoints(minDate, maxDate, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        listBean.forEach(orderBean -> summaryBean.setTotalEarning(summaryBean.getTotalEarning() + orderBean.getPartnerCharge()));
        summaryBean.setNumberOfDelivery(listBean.size());
        summaryBean.setTotalServiceCharge(0d);
        summaryBean.setDate(date);
        return summaryBean;
    }

    public DealerTransactionSummaryBean getDealerTransactionSummaryMonth(String id, String date) {
        DealerTransactionSummaryBean dealerTransactionSummaryBean = new DealerTransactionSummaryBean();
        Customer dealer = customerService.getRepository().findById(id).get();
        List<OrderInfo> list = orderRepository.findByDealer(dealer, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<OrderBean> listBean = new ArrayList<>();
        LocalDate dateTime = LocalDate.parse(date);
        LocalDate minDate = dateTime.withDayOfMonth(1);
        LocalDate maxDate = dateTime.withDayOfMonth(dateTime.getMonth().length(dateTime.isLeapYear()));
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateInBetweenIncludingEndPoints(minDate, maxDate, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBean.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        Map<String, List<OrderBean>> orderInfolistGrouped = listBean.stream().collect(Collectors.groupingBy(orderBean -> orderBean.getOrderProductList().get(0).getBuyProduct().getBrand().getNameEn()));
        dealerTransactionSummaryBean.setOrderInfolistGrouped(orderInfolistGrouped);
        dealerTransactionSummaryBean.setTotal(listBean.size());
        dealerTransactionSummaryBean.setDate(date);
        return dealerTransactionSummaryBean;
    }

    public PartnerDashboardBean getPartnerDashboard(String id) {
        Customer partner = customerService.getRepository().findById(id).get();
        PartnerDetailsBean partnerDetails = partnerDetailsService.getPartnerDetails(id);
        PartnerDashboardBean partnerDashboardBean = new PartnerDashboardBean();
        partnerDashboardBean.setPayable(partnerDetails.getTotalPayable());
        partnerDashboardBean.setTotalEarning(partnerDetails.getTotalEarnings());
        partnerDashboardBean.setMonthlyEarning(0d);
        partnerDashboardBean.setLastOrderEarnings(0d);
        partnerDashboardBean.setWeeklyEarning(0d);
        List<OrderInfo> list = orderRepository.findByPartnerId(id, Sort.by(Sort.Direction.DESC, "dateCreated"));
        List<OrderBean> listBeanMonthly = new ArrayList<>();
        List<OrderBean> allCompletedOrder = new ArrayList<>();
        LocalDate dateTime = LocalDate.now();
        LocalDate minDate = dateTime.withDayOfMonth(1);
        LocalDate minYear = dateTime.withDayOfYear(1);
        LocalDate maxDate = dateTime.withDayOfMonth(dateTime.getMonth().length(dateTime.isLeapYear()));
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateAfter(minDate, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBeanMonthly.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateAfter(minYear, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                allCompletedOrder.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        Map<Month, Long> actualByMonth = allCompletedOrder.stream().filter(orderBean -> orderBean.getAcceptedAt() != null && orderBean.getReceivedAt() != null).collect(Collectors.groupingBy(m -> Month.from(DateUtils.getLocalDateTimeFromInstant(m.getDateCreated())), TreeMap::new, Collectors.summingLong(m -> {
            long minutes = Duration.between(m.getAcceptedAt(), m.getReceivedAt()).toMinutes();
            System.out.println(minutes);
            return minutes;
        })));

        Map<Month, Integer> calculatedByMonth = allCompletedOrder.stream().collect(Collectors.groupingBy(m -> Month.from(DateUtils.getLocalDateTimeFromInstant(m.getDateCreated())), TreeMap::new, Collectors.summingInt(m -> 60)));
        partnerDashboardBean.setActualByMonth(actualByMonth);
        partnerDashboardBean.setCalculatedByMonth(calculatedByMonth);

        listBeanMonthly.forEach(orderBean -> partnerDashboardBean.setMonthlyEarning(partnerDashboardBean.getMonthlyEarning() + orderBean.getPartnerCharge()));
        if (listBeanMonthly.size() > 0) {
            partnerDashboardBean.setLastOrderEarnings(listBeanMonthly.get(0).getPartnerCharge());
        }
        partnerDashboardBean.setRewardPoint(partner.getRewardPoint());

        LocalDate then = dateTime.minusDays(7);
        List<OrderBean> listBeanWeekly = new ArrayList<>();
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateInBetweenIncludingEndPoints(then, minDate, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBeanWeekly.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        listBeanWeekly.forEach(orderBean -> partnerDashboardBean.setWeeklyEarning(partnerDashboardBean.getWeeklyEarning() + orderBean.getPartnerCharge()));
        return partnerDashboardBean;
    }

    public DealerDashboardBean getDealerDashboard(String id, String date) {
        Customer dealer = customerService.getRepository().findById(id).get();
        DealerDashboardBean dealerDashboardBean = new DealerDashboardBean();
        dealerDashboardBean.setTotalSalesThisMonth(0d);
        List<OrderInfo> list = orderRepository.findByDealer(dealer, Sort.by(Sort.Direction.DESC, "dateCreated"));
        dealerDashboardBean.setOrderReceived(list.size());
        dealerDashboardBean.setTotalDeliveryVsOrder(100d);
        dealerDashboardBean.setDeliveredThisMonth(0);
        List<OrderBean> listBeanMonthly = new ArrayList<>();
        List<OrderBean> allCompletedOrder = new ArrayList<>();
        List<OrderBean> chartListBeanMonthly = new ArrayList<>();
        LocalDate dateTime = LocalDate.now();
        LocalDate dateChart = LocalDate.parse(date);
        LocalDate minDate = dateTime.withDayOfMonth(1);
        LocalDate minDateChart = dateChart.withDayOfMonth(1);
        LocalDate minYear = minDate.withDayOfYear(1);
        LocalDate maxDateChart = dateChart.withDayOfMonth(dateChart.getMonth().length(dateChart.isLeapYear()));
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateAfter(minDate, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                listBeanMonthly.add(getModelMapper().map(orderInfo, OrderBean.class));
                dealerDashboardBean.setTotalSalesThisMonth(dealerDashboardBean.getTotalSalesThisMonth() + orderInfo.getSubTotal());
            }
        });
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateInBetweenIncludingEndPoints(minDateChart, maxDateChart, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                chartListBeanMonthly.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        list.forEach(orderInfo -> {
            String orderStatus = orderInfo.getOrderStatus();
            if (orderStatus.equalsIgnoreCase(Constant.COMPLETED) && DateUtils.isDateAfter(minYear, DateUtils.getLocalDateTimeFromInstant(orderInfo.getDateCreated()))) {
                allCompletedOrder.add(getModelMapper().map(orderInfo, OrderBean.class));
            }
        });
        Map<MonthDay, Double> salesDayOnMonth = chartListBeanMonthly.stream().collect(Collectors.groupingBy(m -> MonthDay.from(DateUtils.getLocalDateTimeFromInstant(m.getDateCreated())), TreeMap::new, Collectors.summingDouble(OrderBean::getSubTotal)));
        Map<Month, Double> salesOnMonth = allCompletedOrder.stream().collect(Collectors.groupingBy(m -> Month.from(DateUtils.getLocalDateTimeFromInstant(m.getDateCreated())), TreeMap::new, Collectors.summingDouble(OrderBean::getSubTotal)));
        dealerDashboardBean.setSalesDayOnMonth(salesDayOnMonth);
        dealerDashboardBean.setSalesOnMonth(salesOnMonth);
        dealerDashboardBean.setDeliveredThisMonth(listBeanMonthly.size());
        dealerDashboardBean.setDate(date);
        return dealerDashboardBean;
    }


    public Object orderTrace(String id) {
        OrderInfo orderInfo = getRepository().getById(id);
        if (!orderInfo.getOrderStatus().equalsIgnoreCase("PICKED")) {
            throw new ServiceExceptionHolder.CustomException(AppConstant.NOT_FOUND_EXCEPTION_CODE, "This order is not in picked state");
        }
        CustomerBean customerBean = customerService.getByOid(orderInfo.getPartner().getId());
        return OrderTraceResponseBean.builder().customer(LatLongBean.builder().longitude(String.valueOf(customerBean.getLongitude())).latitude(String.valueOf(customerBean.getLatitude())).build()).partner(LatLongBean.builder().longitude(String.valueOf(orderInfo.getDeliveryLong())).latitude(String.valueOf(orderInfo.getDeliveryLat())).build()).build();
    }

    public List<OrderInfo> getAllOrderByCustomerId(String id) {
        return ((OrderRepository) getRepository()).findByCustomerId(id, Sort.by(Sort.Order.desc("dateCreated")));
    }

    public void sendOderNotificationToPartnerAndCustomer(OrderInfo orderInfo) {
        List<PartnerDetailsBean> nearByPartnerList = getNearByPartner(orderInfo, 0);
        log.info("Partner-found=============>>>{}", nearByPartnerList.size());
        OrderInfo createdOrderInfo = null;
        if (nearByPartnerList.size() > 0) {
            log.info("Partner-found=============>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getId());
            log.info("Partner-found=============>>>{}", nearByPartnerList.get(0).getCustomerInfoBean().getCustomerName());
            orderInfo.setPartner(customerService.getRepository().getById(nearByPartnerList.get(0).getCustomerInfoBean().getId()));
            orderInfo.setOrderStatus(Constant.PENDING);
            orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
            createdOrderInfo = getRepository().save(orderInfo);
            appNotificationService.sendOrderActionNotification(orderInfo, nearByPartnerList.get(0));
        } else {
            orderInfo.setOrderStatus(Constant.NO_USER_FOUND);
            orderInfo.setPartner(null);
            orderInfo.setNoOfTry(orderInfo.getNoOfTry() + 1);
            createdOrderInfo = getRepository().save(orderInfo);
        }
        orderHistoryService.saveHistory(getOrderHistoryBean(createdOrderInfo, createdOrderInfo.getOrderStatus()));
    }

    public Object updateOrderStatus(String id, OrderStatusBean statusBean) {
        OrderInfo orderInfo = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No id found"));
        if (statusBean.getStatus().equalsIgnoreCase(Constant.RECEIVED)) {
            orderInfo.setOrderStatus(Constant.RECEIVED);
            update(id, orderInfo);
            OrderHistory historyBean = getOrderHistoryBean(orderInfo, orderInfo.getOrderStatus());
            historyBean.setPartner(orderInfo.getCustomer());
            orderHistoryService.saveHistory(historyBean);
        } else if (statusBean.getStatus().equalsIgnoreCase(Constant.CANCELED)) {
            orderInfo.setOrderStatus(Constant.CANCELED);
            update(id, orderInfo);
            OrderHistory historyBean = getOrderHistoryBean(orderInfo, orderInfo.getOrderStatus());
            historyBean.setPartner(orderInfo.getCustomer());
            orderHistoryService.saveHistory(historyBean);
        }

        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public Double getAverageTime(Customer customer) {
        return orderRepository.findByPartnerId(customer.getId(), Sort.by(Sort.Direction.DESC, "dateCreated"))
                .stream()
                .filter(orderBean -> Objects.nonNull(orderBean.getAcceptedAt()) && Objects.nonNull(orderBean.getReceivedAt()))
                .filter(o -> o.getOrderStatus().equalsIgnoreCase(Constant.COMPLETED))
                .map(o -> Duration.between(o.getAcceptedAt(), o.getReceivedAt()).toMinutes())
                .mapToDouble(i -> i).average().orElse(0.0);
    }

//    public Double getAverageTime(Customer customer) {
//        return orderRepository.findByPartnerId(customer.getId(), Sort.by(Sort.Direction.DESC, "dateCreated"))
//                .stream()
//                .filter(orderBean -> orderBean.getAcceptedAt() != null && orderBean.getReceivedAt() != null).filter(o -> o.getOrderStatus().equalsIgnoreCase(Constant.COMPLETED))
//                .map(o -> Duration.between(o.getAcceptedAt(), o.getReceivedAt()).toMinutes())
//                .mapToDouble(i -> i).average().orElse(0.0);
//
//    }


    public Integer getCompletedOrder(Customer customer) {
        return orderRepository.findByPartnerId(customer.getId(), Sort.by(Sort.Direction.DESC, "dateCreated")).size();
    }
}
