package com.ba.gas.monkey.services;

import com.ba.gas.monkey.base.BaseRepository;
import com.ba.gas.monkey.base.BaseService;
import com.ba.gas.monkey.constants.AppConstant;
import com.ba.gas.monkey.dtos.ClusterBean;
import com.ba.gas.monkey.dtos.CommonResponseBean;
import com.ba.gas.monkey.dtos.DropdownDTO;
import com.ba.gas.monkey.dtos.cluster.ClusterCreateBean;
import com.ba.gas.monkey.dtos.cluster.ClusterListBean;
import com.ba.gas.monkey.exception.ServiceExceptionHolder;
import com.ba.gas.monkey.models.Cluster;
import com.ba.gas.monkey.models.Thana;
import com.ba.gas.monkey.repositories.ClusterRepository;
import com.ba.gas.monkey.repositories.ThanaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("clusterService")
public class ClusterService extends BaseService<Cluster, ClusterBean> {

    private final ThanaService thanaService;

    public ClusterService(BaseRepository<Cluster> repository, ModelMapper modelMapper, ThanaService thanaService) {
        super(repository, modelMapper);
        this.thanaService = thanaService;
    }

    public List<DropdownDTO> getDropdownList(String thanaId) {
        Thana thana = thanaService.getRepository().findById(thanaId).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No thana found by id :" + thanaId));
        List<Cluster> clusters = ((ClusterRepository) getRepository())
                .findByThana(thana)
                .stream()
                .filter(Cluster::getStatus)
                .sorted(Comparator.comparing(Cluster::getName))
                .collect(Collectors.toUnmodifiableList());
        return clusters.stream().map(this::getDropdownDTO).collect(Collectors.toUnmodifiableList());
    }

    private DropdownDTO getDropdownDTO(Cluster cluster) {
        DropdownDTO dto = getModelMapper().map(cluster, DropdownDTO.class);
        dto.setValue(cluster.getName());
        return dto;
    }

    public Page<ClusterListBean> getClusterList(Pageable pageable, String thanaId) {
        Thana thana = thanaService.getRepository()
                .findById(thanaId)
                .orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No thana found by id :" + thanaId));

        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(),pageable.getPageSize(), Sort.by(Sort.Order.desc("dateCreated")));
        Page<Cluster> page = ((ClusterRepository) getRepository())
                .findByThana(thana, pageRequest);

        return new PageImpl<>(
                page.getContent()
                        .stream()
                        .map(this::getClusterListBean)
                        .sorted(Comparator.comparing(ClusterListBean::getStatus).reversed())
                        .collect(Collectors.toUnmodifiableList()),page.getPageable(),page.getTotalElements());
    }

    private ClusterListBean getClusterListBean(Cluster cluster) {
        ClusterListBean bean = getModelMapper().map(cluster,ClusterListBean.class);
        bean.setStatus(cluster.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE);
        return bean;
    }

    public Object createCluster( @RequestBody @Validated ClusterCreateBean bean) {
        Cluster cluster = getModelMapper().map(bean, Cluster.class);
        cluster.setThana(thanaService.getRepository().getById(bean.getThanaId()));
        return create(cluster);
    }

    public Object deleteCluster(String id) {
        Cluster cluster = getRepository().getById(id);
        cluster.setStatus(Boolean.FALSE);
        update(id,cluster);
        return CommonResponseBean.builder().msg(AppConstant.SUCCESS).build();
    }

    public Object updateCluster(String id, ClusterCreateBean bean) {
        Cluster cluster = getRepository().getById(id);
        BeanUtils.copyProperties(bean,cluster, AppConstant.IGNORE_PROPERTIES);
        return update(id,cluster);
    }
}
